#!/usr/bin/env python3
"""
Urwid interface for equipy
"""
# Imports {{{
import urwid
import urwid.raw_display as rawd
import urwidtrees
from widgets import *

import cacautils

import sys, argparse
import numpy as np
import subprocess
from subprocess import call
from subprocess import call
import platform
import time
from time import sleep
from collections import deque
from math import floor
import threading
import curses, os #curses is the interface for capturing key presses on the menu, os launches the files

from equipy import *
# }}}

global ExitFlag 
ExitFlag = 0

displ = None
current_stash = None
current_list = None
current_backpack = None

class SumListsDialog(OkCancelDialog): # {{{
    signals = ['sumlists_close_ok','close_cancel']
    _focus_list = []
    def __init__(self, edit = False):
        global current_stash
        global current_list
        self._focus_list = [ HighlightableItem(l_name, checkable = True) for l_name in current_stash.lists.keys() if l_name != current_list ]
        for l in self._focus_list:
            if l._text in current_stash.lists[current_list].sumlists:
                l._checked = True

        self.w_listnames = urwid.BoxAdapter(urwid.ListBox(urwid.SimpleFocusListWalker(self._focus_list)),5)
        super(SumListsDialog, self).__init__('Select lists to add', [self.w_listnames])
        urwid.connect_signal(self.w_ok_button, 'click', lambda action: self._emit('sumlists_close_ok'))

# }}}

class ChooseListDialog(NoButtonsDialog): # {{{
    signals = ['chooselist_close_ok','close_dialog']
    _focus_list = []
    _chosen_list = ""
    def __init__(self, height = 6):
        global current_stash
        global current_list
        self._focus_list = [ HighlightableItem(l_name, checkable = False) for l_name in current_stash.lists.keys() if l_name != current_list ]
        self.w_listnames = urwid.ListBox(urwid.SimpleFocusListWalker(self._focus_list))
        super(ChooseListDialog, self).__init__('Go to list:', [urwid.BoxAdapter(self.w_listnames,height)])
        #urwid.connect_signal(self, 'click', lambda action: self._emit('chooselist_close_ok'))
    def keypress(self, size, key):
        self.w_pile.keypress(size,key)
        if key == 'enter' or key == ' ':
            self._chosen_list = self.w_listnames.focus._text
            self._emit('chooselist_close_ok')
        if key == 'esc':
            self._emit('close_dialog')
        #return key
# }}}

class CategoryListBox(urwid.ListBox): # {{{
    def get_next_item(self):
        c_pos = self.body.next_position(self.focus_position)
        while (not self.body[c_pos]._selectable):
            c_pos = self.body.next_position(c_pos)
        return self.body[c_pos]._id
        #IndexError will be thrown automatically in case something goes wrong
    def get_prev_item(self):
        c_pos = self.body.prev_position(self.focus_position)
        while (not self.body[c_pos]._selectable):
            c_pos = self.body.prev_position(c_pos)
        return self.body[c_pos]._id
        #IndexError will be thrown automatically in case something goes wrong
        return
    def next_available_item(self):
        try:
            next_item_uuid = self.get_next_item()
        except IndexError:
            try:
                next_item_uuid = self.get_prev_item()
            except IndexError:
                next_item_uuid = -1
        return next_item_uuid
    def focus_on_item(self, item_uuid):
        global current_stash
        if item_uuid is None or item_uuid == -1:
            return False
        for p in self.body.positions():
            if self.body[p]._id == item_uuid:
                self.set_focus(p)
                pinfo(current_stash.items[item_uuid].description)
                return True
        return False
    def keypress(self,size,key):
        super(CategoryListBox,self).keypress(size,key)
        if self.focus is not None:
            if not self.focus._id == -1:
                pinfo(current_stash.items[self.focus._id].description)
        return key
    def mouse_event(self, size, event, button, col, row, focus):
        if event == 'mouse press':
            if button == 4: #scroll up
                r = self.keypress(size, 'up')
                return True
            if button == 5: #scroll down
                r = self.keypress(size, 'down')
                return True
            else:
                super(CategoryListBox,self).mouse_event(size, event, button, col, row, focus)
        return False
# }}}

class CategoryTreeBox(urwidtrees.TreeBox): # {{{
    def __init__(self, tree):
        super(CategoryTreeBox,self).__init__(tree)

    def get_next_item(self):
        c_pos = self._tree.next_position(self._walker._focus)
        if not c_pos in self._walker.positions():
            raise IndexError
        while (self._walker[c_pos]._id == -1 ):
            c_pos = self._tree.next_position(c_pos)
        return self._walker[c_pos]._id
        #IndexError will be thrown automatically in case something goes wrong
    def get_prev_item(self):
        c_pos = self._tree.prev_position(self._walker._focus)
        if not c_pos in self._walker.positions():
            raise IndexError
        while (self._walker[c_pos]._id == -1):
            c_pos = self._tree.prev_position(c_pos)
        return self._walker[c_pos]._id
        #IndexError will be thrown automatically in case something goes wrong
        return
    def next_available_item(self):
        try:
            next_item_uuid = self.get_next_item()
        except IndexError:
            try:
                next_item_uuid = self.get_prev_item()
            except IndexError:
                next_item_uuid = -1
        return next_item_uuid

    def focus_on_item(self, item_uuid):
        global current_stash

        if item_uuid is None or item_uuid == -1:
            return False

        item_cat = current_stash.items[item_uuid].category
        cat_pos = (0,) 
        while self._walker[cat_pos]._text != item_cat:
            cat_pos = self._tree.next_sibling_position(cat_pos)
        self._tree.expand(cat_pos)

        for p in self._walker.positions():
            if self._walker[p]._id == item_uuid:
                self.set_focus(p)
                pinfo(current_stash.items[item_uuid].description)
                #return True
        return False
    def keypress(self,size,key):
        #fd = open('./log.txt','a')
        #fd.write(str(self.get_focus()[0]._id) + '\n')
        #fd.close()
        super(CategoryTreeBox,self).keypress(size,key)
        if self.get_focus()[0] is not None:
            if not self.get_focus()[0]._id == -1:
                pinfo(current_stash.items[self.get_focus()[0]._id].description)
        return key
    #    global current_stash
    def mouse_event(self, size, event, button, col, row, focus):
        if event == 'mouse press':
            if button == 4: #scroll up
                r = self.keypress(size, 'up')
                return True
            if button == 5: #scroll down
                r = self.keypress(size, 'down')
                return True
            else:
                return self._outer_list.mouse_event(size, event, button, col, row, focus)
        return False
# }}}

class StashWidget(CategoryTreeBox): # {{{
    signals = ['newstashitem','editstashitem','removestashitem','savestash','newlist','newbackpack','chooselist','sumlists','removelist','removebackpack','help']
    def __init__(self, tree):
        super(StashWidget,self).__init__(tree)
        urwid.register_signal(StashWidget, self.signals)

    #def keypress(self,size,key):
    #    global current_stash
    #    global current_list
    #    global current_backpack
    #    global displ
    #    return super(StashWidget,self).keypress(size,key)

        #if self.get_focus()[0]._id != -1:
        #    if key == 'enter' or key == ' ':
        #        current_stash.add_to_bp(current_list, current_backpack, self.get_focus()[0]._id)
        #        displ.w_backpack.body = displ.populate_backpack()
        #        displ.w_backpack._invalidate()
        #    #elif key == 'del':
        #    elif key == 'a':
        #        #new item
        #        displ.w_stash_itemdialog._edit_item = current_stash.items[self.get_focus()[0]._id]
        #        self._emit("newstashitem")
        #    elif key == 'e':
        #        displ.w_stash_itemdialog._edit_item = current_stash.items[self.get_focus()[0]._id]
        #        #edit item
        #        self._emit("editstashitem")
        #    elif key == 'r' or key == 'del' or key == 'backspace':
        #        displ.w_stash_itemdialog._edit_item = current_stash.items[self.get_focus()[0]._id]
        #        self._emit("removestashitem")
        #    if self.get_focus()[0] is not None:
        #        displ.w_backpack.focus_on_item(self.get_focus()[0]._id)
        #return key

# }}}

class StashItemWidget(urwid.Widget): # {{{
    _selectable = True
    _sizing = frozenset('box')
    _id = 0
    _text = 'empty'
    _color = 'default'
    def __init__(self, text, item_id, color = 'default'):
        global current_stash
        item = current_stash.items[item_id]
        self._id = item_id
        self._text = text
        if item.weight > 0:
            if item.color is not None:
                self._color = item.color
            else:
                self._color = color
        else:
            self._color = 'dark red'
    def rows(self, size, focus = False):
        global current_stash
        item = current_stash.items[self._id]
        tmp_w = urwid.Columns([
                ('weight',1,urwid.Text(self._text,wrap='any')),
                ('pack',urwid.Text('  ' + str(int(item.weight)) + ' g',wrap='any'))
                ])
        return tmp_w.render(size).rows() 
    def render(self, size, focus = False):
        global current_stash
        item = current_stash.items[self._id]
        tmp_w = urwid.Columns([
                ('weight',1,urwid.Text(self._text,wrap='any')),
                ('pack',urwid.Text('  ' + str(int(item.weight)) + ' g',wrap='any'))
                ])
        if self._color == 'default' or self._color == '' or self._color is None:
            new_attr = 'item not selected'
        else:
            try:
                new_attr = urwid.AttrSpec(self._color, 'default')
            except urwid.display_common.AttrSpecError:
                new_attr = 'item not selected'
                pass
        return urwid.AttrMap(tmp_w, new_attr, focus_map='item selected').render(size, focus = focus)
    def keypress(self, size, key):
        #pinfo("keypress test - " + self._text)
        #if self.get_focus()[0]._id != -1:
        global displ
        global current_list
        global current_backpack
        global current_stash
        return_key = key
        if self._id != -1:
            if key == 'enter' or key == ' ':
                current_stash.add_to_bp(current_list, current_backpack, self._id)
                displ.w_backpack.body = displ.populate_backpack()
                displ.w_backpack._invalidate()
                return_key = ''
            elif key == 'a':
                #new item
                displ.w_stash_itemdialog._edit_item = current_stash.items[self._id]
                displ.w_stash._emit("newstashitem")
                return_key = ''
            elif key == 'e':
                displ.w_stash_itemdialog._edit_item = current_stash.items[self._id]
                #edit item
                displ.w_stash._emit("editstashitem")
                return_key = ''
            elif key == 'r' or key == 'del' or key == 'backspace':
                displ.w_stash_itemdialog._edit_item = current_stash.items[self._id]
                displ.w_stash._emit("removestashitem")
                return_key = ''
            displ.w_backpack.focus_on_item(self._id)
        return return_key
    def mouse_event(self, size, event, button, col, row, focus):
        if event == 'mouse press':
            if button == 1:
                displ.w_backpack.focus_on_item(self._id)
                return True;
        return False
# }}}

class StashCatWidget(urwid.Widget): # {{{
    _selectable = True
    _sizing = frozenset('box')
    _text = 'empty'
    _id = -1
    def __init__(self, text):
        self._text = text
    def rows(self, size, focus = False):
        return 1
    def render(self, size, focus = False):
        return urwid.AttrMap(urwid.Text('-: ' + self._text + ' :-',align='center',wrap='clip'),'category not selected', focus_map='category selected').render(size, focus)
    def keypress(self, size, key):
        return key
# }}}

class BackpackWidget(CategoryListBox): # {{{

    def keypress(self,size,key):
        return super(BackpackWidget,self).keypress(size,key)

# }}}

class BackpackItemWidget(urwid.WidgetWrap): # {{{

    _selectable = True
    _sizing = frozenset('box')
    _id = 0
    _quantity = 1
    _text = 'empty'
    _from_list = None
    def __init__(self, item_id, quantity, selectable = True, from_list = None):
        self._id = item_id
        item = current_stash.items[item_id]
        self._text = str(quantity) + 'x ' + item.name 
        self._quantity = quantity
        self._selectable = selectable
        self._from_list = from_list
        _w_add = BackpackItemAddWidget(self._id)
        _w_remove = BackpackItemRemoveWidget(self._id)

        item = current_stash.items[self._id]

        tmp_text = self._text
        if self._from_list is not None:
            tmp_text = " (From " + self._from_list + ") " + tmp_text
        else:
            tmp_text = " " + tmp_text

        _w_weight = urwid.Text('  ' + str(int(item.weight)*self._quantity) + ' g',wrap='clip')
        super(BackpackItemWidget, self).__init__(urwid.Columns([
                                                            (5,_w_add),
                                                            (5,_w_remove),
                                                            ('weight',1,urwid.Text(tmp_text,wrap='clip')),
                                                            (10,_w_weight)
                                                            ]))
            
    def rows(self, size, focus = False):
        return 1
    
    def get_cursor_coords(self, size):
        return None
    def render(self, size, focus = False):
       
        if not self._selectable:
            if self._from_list is None:
                return urwid.AttrMap(self._w,'item not selectable').render(size)
            else:
                return urwid.AttrMap(self._w,urwid.AttrSpec('light blue', 'default')).render(size)
        else:
            return urwid.AttrMap(self._w,'item not selected', focus_map = 'item selected').render(size, focus)

    def keypress(self, size, key):
        global current_stash
        global current_list
        global current_backpack
        global displ
        #if self.focus is not None:
        current_item_uuid = self._id
        try:
            alt_item_uuid = displ.w_backpack.get_next_item()
        except IndexError:
            try:
                alt_item_uuid = displ.w_backpack.get_prev_item()
            except IndexError:
                alt_item_uuid = -1

        if key == 'enter' or key == '-' or key == ' ':
            if current_stash.lists[current_list].has_item(current_backpack,self._id, 'own'):
                current_stash.remove_from_bp(current_list, current_backpack,self._id)
                displ.w_backpack.body = displ.populate_backpack()
        if key == '+':
            if current_stash.lists[current_list].has_item(current_backpack,self._id, 'own'):
                current_stash.add_to_bp(current_list, current_backpack, self._id)
                displ.w_backpack.body = displ.populate_backpack()

        displ.w_backpack._invalidate()
        if not displ.w_backpack.focus_on_item(current_item_uuid):
            if alt_item_uuid != -1:
                displ.w_backpack.focus_on_item(alt_item_uuid)
        if self.focus is not None:
            displ.w_stash.focus_on_item(self.focus._id)
        return key

    def mouse_event(self, size, event, button, col, row, focus):
        global displ
        pinfo('working')
        if event == 'mouse press':
            if button == 1:
                pinfo('working')
                displ.w_stash.focus_on_item(self._id)
                if hasattr(self._w,'mouse_event'):
                    return self._w.mouse_event(size, event, button, col, row, focus);
        return False
# }}}

class BackpackItemAddWidget(TextButton): # {{{
    _id = 0 
    def __init__ (self,item_id):
        super(BackpackItemAddWidget,self).__init__("( + )")
        self._id = item_id;
    def mouse_event(self, size, event, button, col, row, focus):
        global current_stash
        global current_list
        global current_backpack
        global displ
        if current_stash.lists[current_list].has_item(current_backpack,self._id, 'own'):
            current_stash.add_to_bp(current_list, current_backpack, self._id)
            displ.w_backpack.body = displ.populate_backpack()
            return True
        return False
# }}}

class BackpackItemRemoveWidget(TextButton): # {{{
    _id = 0 
    def __init__ (self,item_id):
        super( BackpackItemRemoveWidget,self).__init__("( - )")
        self._id = item_id;
    def mouse_event(self, size, event, button, col, row, focus):
        global current_stash
        global current_list
        global current_backpack
        global displ
        if current_stash.lists[current_list].has_item(current_backpack,self._id, 'own'):
            current_stash.remove_from_bp(current_list, current_backpack,self._id)
            displ.w_backpack.body = displ.populate_backpack()
            return True
        return False
# }}}

class BackpackCatWidget(urwid.Widget): # {{{
    _selectable = False
    _sizing = frozenset('box')
    _text = 'Empty'
    _id = -1
    def __init__(self, text):
        self._text = text
    def rows(self, size, focus = False):
        return 1
    def render(self, size, focus = False):
        return urwid.AttrMap(urwid.Text('-: ' + self._text + ' :-',align='center',wrap='clip'),'category not selected').render(size)
    def keypress(self, size, key):
        return key
# }}}

class HelpDialog(InfoDialog): # {{{
    def __init__(self):
        rjustlen = 20
        ljustlen = 30
        help_text = "up/down/left/right ".rjust(rjustlen) + "| general movement".ljust(ljustlen) + "\n" + \
        "space ".rjust(rjustlen) + "| add/remove item from backpack".ljust(ljustlen) + "\n" + \
        "L ".rjust(rjustlen) + "| new list".ljust(ljustlen) + "\n" + \
        "R ".rjust(rjustlen) + "| remove list".ljust(ljustlen) + "\n" + \
        "n ".rjust(rjustlen) + "| next list".ljust(ljustlen) + "\n" + \
        "b ".rjust(rjustlen) + "| previous list".ljust(ljustlen) + "\n" + \
        "S ".rjust(rjustlen) + "| select list to sum".ljust(ljustlen) + "\n" + \
        "G ".rjust(rjustlen) + "| go to list".ljust(ljustlen) + "\n" + \
        "B ".rjust(rjustlen) + "| new backpack".ljust(ljustlen) + "\n" + \
        "r ".rjust(rjustlen) + "| remove backpack".ljust(ljustlen) + "\n" + \
        "tab ".rjust(rjustlen) + "| cycles backpacks".ljust(ljustlen) + "\n" + \
        "s ".rjust(rjustlen) + "| save stash and lists".ljust(ljustlen) + "\n" + \
        "? ".rjust(rjustlen) + "| show command list".ljust(ljustlen) + "\n" + \
        "q ".rjust(rjustlen) + "| quit".ljust(ljustlen) + "\n" + \
        "\n" + \
        "------ on stash ------" + "\n" + \
        "\n" + \
        "a ".rjust(rjustlen) + "| add item".ljust(ljustlen) + "\n" + \
        "e ".rjust(rjustlen) + "| edit item".ljust(ljustlen) + "\n" + \
        "C ".rjust(rjustlen) + "| collapse all categories".ljust(ljustlen) + "\n" + \
        "E ".rjust(rjustlen) + "| expand all categories".ljust(ljustlen) + "\n" + \
        "backspacke/del/r ".rjust(rjustlen) + "| remove item".ljust(ljustlen) + "\n" + \
        "------ on backpack ------" + "\n" + \
        "\n" + \
        "+\- ".rjust(rjustlen) + "| increase\decrease item count".ljust(ljustlen)  
        super(HelpDialog, self).__init__("Command List", help_text)
# }}}

class StashPopUp(urwid.PopUpLauncher): # {{{
    _edit_item = None
    def __init__(self, widget):
        super(StashPopUp, self).__init__(widget)
        #print(self.original_widget.signals)
        urwid.connect_signal(self.original_widget,'newstashitem',
                lambda action: self.open_new_item())
        urwid.connect_signal(self.original_widget,'editstashitem',
                lambda action: self.open_edit_item())
        urwid.connect_signal(self.original_widget,'savestash',
                lambda action: self.open_confirm_save())
        urwid.connect_signal(self.original_widget,'newlist',
                lambda action: self.open_new_list())
        urwid.connect_signal(self.original_widget,'newbackpack',
                lambda action: self.open_new_backpack())
        urwid.connect_signal(self.original_widget,'chooselist',
                lambda action: self.open_choose_list())
        urwid.connect_signal(self.original_widget,'sumlists',
                lambda action: self.open_sum_lists())
        urwid.connect_signal(self.original_widget,'removestashitem',
                lambda action: self.open_confirm_delete_item())
        urwid.connect_signal(self.original_widget,'removelist',
                lambda action: self.open_confirm_delete_list())
        urwid.connect_signal(self.original_widget,'removebackpack',
                lambda action: self.open_confirm_delete_backpack())
        urwid.connect_signal(self.original_widget,'help',
                lambda action: self.open_help())
        self._edit_item = None
    def create_pop_up(self):
        return self._pop_up_widget 
    def get_pop_up_parameters(self):
        global displ
        
        (s_width, s_height) = urwid.raw_display.Screen().get_cols_rows()
        left = floor((s_width - self.pop_w)/2)
        top = floor((s_height - self.pop_h)/2)
        return {'left':left, 'top':top, 'overlay_width':self.pop_w, 'overlay_height':self.pop_h}
    
    def open_confirm_save(self): # {{{
        self._pop_up_widget = YesNoDialog("Save stash and lists?")
        urwid.connect_signal(self._pop_up_widget, 'yesno_close_yes',
                lambda action: self.save_stash_yes())
        urwid.connect_signal(self._pop_up_widget, 'yesno_close_no',
                lambda action: self.close_pop_up())
        (self.pop_w,self.pop_h) = (30,5)
        self.open_pop_up()
        # }}}
    def save_stash_yes(self): # {{{
        save_stash()
        self.close_pop_up()
        # }}}
        
    def open_confirm_delete_item(self): # {{{
        self._pop_up_widget = YesNoDialog("Really delete item?")
        urwid.connect_signal(self._pop_up_widget, 'yesno_close_yes',
                lambda action: self.remove_item_yes())
        urwid.connect_signal(self._pop_up_widget, 'yesno_close_no',
                lambda action: self.close_pop_up())
        (self.pop_w,self.pop_h) = (30,5)
        self.open_pop_up()
        # }}}
    def remove_item_yes(self): # {{{
        global current_stash
        global displ
        next_item_uuid = self.original_widget.next_available_item()
        current_stash.remove_item(self._edit_item.uuid)
        #self.original_widget.body = displ.populate_stash()
        self.original_widget.__init__( displ.populate_stash())
        #displ.w_stash = StashWidget(self.populate_stash())
        displ.w_backpack.body = displ.populate_backpack()
        if next_item_uuid != -1:
            self.original_widget.focus_on_item(next_item_uuid)
            if not displ.w_backpack.focus_on_item(next_item_uuid):
                next_bp_item = displ.w_backpack.next_available_item()
                displ.w_backpack.focus_on_item(next_bp_item)
        self.original_widget._invalidate()
        displ.w_backpack._invalidate()
        self.close_pop_up()
        # }}}

    def open_confirm_delete_list(self): # {{{
        self._pop_up_widget = YesNoDialog("Really delete list?")
        urwid.connect_signal(self._pop_up_widget, 'yesno_close_yes',
                lambda action: self.remove_list_yes())
        urwid.connect_signal(self._pop_up_widget, 'yesno_close_no',
                lambda action: self.close_pop_up())
        (self.pop_w,self.pop_h) = (30,5)
        self.open_pop_up()
        # }}}
    def remove_list_yes(self): # {{{
        global current_stash
        global current_list
        global displ
        if len(current_stash.lists) > 1:
            prev_list = current_list
            next_list(+1)
            current_stash.remove_list(prev_list)
        else:
            current_stash.remove_list(current_list)
            current_list = None
            displ.w_backpack.body = displ.populate_backpack()
        self.close_pop_up()
        # }}}

    def open_confirm_delete_backpack(self): # {{{
        self._pop_up_widget = YesNoDialog("Really delete backpack?")
        urwid.connect_signal(self._pop_up_widget, 'yesno_close_yes',
                lambda action: self.remove_backpack_yes())
        urwid.connect_signal(self._pop_up_widget, 'yesno_close_no',
                lambda action: self.close_pop_up())
        (self.pop_w,self.pop_h) = (30,5)
        self.open_pop_up()
        # }}}
    def remove_backpack_yes(self): # {{{
        global current_stash
        global current_list
        global current_backpack
        global displ
        if current_backpack == 'common_items':
            current_stash.remove_bp(current_list, current_backpack)
            displ.w_backpack.body = displ.populate_backpack()
            displ.w_backpack._invalidate()
        elif len(current_stash.lists[current_list].backpacks.keys()) > 0:
            prev_bp = current_backpack
            next_backpack(+1)
            current_stash.remove_bp(current_list, prev_bp)
        self.close_pop_up()
        # }}}

    def open_new_item(self): # {{{
        self._pop_up_widget = NewStashItemDialog()
        self._pop_up_widget.w_category.set_edit_text(self._edit_item.category)
        urwid.connect_signal(self._pop_up_widget, 'close_ok',
                lambda action: self.newitem_close_ok())
        urwid.connect_signal(self._pop_up_widget, 'close_cancel',
                lambda action: self.close_pop_up())
        (self.pop_w,self.pop_h) = (40,12)
        self.open_pop_up()
        # }}}
    def newitem_close_ok(self): # {{{
        global current_stash
        name = self._pop_up_widget.w_name.edit_text
        try:
            weight = int(self._pop_up_widget.w_weight.edit_text)
        except:
            weight = 0
            pass
        category = self._pop_up_widget.w_category.edit_text
        description = self._pop_up_widget.w_description.edit_text
        #item_color = self._pop_up_widget.w_color.edit_text
        item_color = ''
        new_item = stash.gearitem(name, category, weight, description, color = item_color)
        current_stash.add_item(new_item)
        #self.original_widget.body = displ.populate_stash()
        self.original_widget.__init__( displ.populate_stash())
        self.original_widget.focus_on_item(new_item.uuid)
        self.original_widget._invalidate()
        self.close_pop_up()
        # }}}

    def open_edit_item(self): # {{{
        self._pop_up_widget = EditStashItemDialog()
        self._pop_up_widget.item_uuid = self._edit_item.uuid
        self._pop_up_widget.w_name.set_edit_text(self._edit_item.name)
        self._pop_up_widget.w_category.set_edit_text(self._edit_item.category)
        self._pop_up_widget.w_weight.set_edit_text(str(int(self._edit_item.weight)))
        self._pop_up_widget.w_description.set_edit_text(self._edit_item.description)
        #if self._edit_item.color is not None:
        #    self._pop_up_widget.w_color.set_edit_text(self._edit_item.color)
        #else:
        #    self._pop_up_widget.w_color.set_edit_text('')
        urwid.connect_signal(self._pop_up_widget, 'close_ok',
                lambda action: self.edititem_close_ok())
        urwid.connect_signal(self._pop_up_widget, 'close_cancel',
                lambda action: self.close_pop_up())
        (self.pop_w, self.pop_h) = (40,12)
        self.open_pop_up()
        # }}}
    def edititem_close_ok(self): # {{{
        global current_stash
        global displ
        name = self._pop_up_widget.w_name.edit_text
        try:
            weight = int(self._pop_up_widget.w_weight.edit_text)
        except:
            weight = 0
            pass
        category = self._pop_up_widget.w_category.edit_text
        description = self._pop_up_widget.w_description.edit_text
        #color = self._pop_up_widget.w_color.edit_text
        color = '' #self._pop_up_widget.w_color.edit_text
        self._edit_item.name = name
        self._edit_item.category = category 
        self._edit_item.weight = weight
        self._edit_item.description = description
        self._edit_item.color = color 
        self.original_widget.__init__( displ.populate_stash())
        self.original_widget.focus_on_item(self._edit_item.uuid)
        bp = current_stash.lists[current_list].get_bp(current_backpack)
        if self._edit_item.uuid in bp.own_items.keys():
            displ.w_backpack.body = displ.populate_backpack()
            displ.w_backpack.focus_on_item(self._edit_item.uuid)
            displ.w_backpack._invalidate()
        #self.original_widget._invalidate()
        displ.w_stash._invalidate()
        self.close_pop_up()
        # }}}

    def open_new_list(self): # {{{
        self._pop_up_widget = NewListDialog()
        urwid.connect_signal(self._pop_up_widget, 'newlist_close_ok',
                lambda action: self.newlist_close_ok())
        urwid.connect_signal(self._pop_up_widget, 'close_cancel',
                lambda action: self.close_pop_up())
        (self.pop_w,self.pop_h) = (40,12)
        self.open_pop_up()
        # }}}
    def newlist_close_ok(self): # {{{
        global current_stash
        global current_list
        name = self._pop_up_widget.w_name.edit_text
        if len(name) > 0:
            current_stash.add_list(name)
            current_list = name
            next_list(0)
        self.close_pop_up()
        # }}}

    def open_new_backpack(self): # {{{
        self._pop_up_widget = NewBackpackDialog()
        urwid.connect_signal(self._pop_up_widget, 'newbackpack_close_ok',
                lambda action: self.newbackpack_close_ok())
        urwid.connect_signal(self._pop_up_widget, 'close_cancel',
                lambda action: self.close_pop_up())
        (self.pop_w,self.pop_h) = (40,12)
        self.open_pop_up()
        # }}}
    def newbackpack_close_ok(self): # {{{
        global current_stash
        global current_list
        global current_backpack
        global displ
        name = self._pop_up_widget.w_name.edit_text
        if len(name) > 0:
            current_stash.lists[current_list].add_bp(name)
            current_backpack = name
            displ.w_backpack.body = displ.populate_backpack()
            displ.w_backpack._invalidate()
            next_list(0)
        self.close_pop_up()
        # }}}
    def open_choose_list(self): # {{{
        self._pop_up_widget = ChooseListDialog(height=8)
        urwid.connect_signal(self._pop_up_widget, 'chooselist_close_ok',
                lambda action: self.choose_list_close_ok())
        urwid.connect_signal(self._pop_up_widget, 'close_dialog',
                lambda action: self.close_pop_up())
        (self.pop_w,self.pop_h) = (40,12)
        self.open_pop_up()
        # }}}
    def choose_list_close_ok(self): # {{{
        global current_stash
        global current_list
        global displ
        current_list = self._pop_up_widget._chosen_list
        displ.w_backpack.body = displ.populate_backpack()
        displ.w_backpack._invalidate()
        self.close_pop_up()
        # }}}
    def open_sum_lists(self): # {{{
        self._pop_up_widget = SumListsDialog()
        urwid.connect_signal(self._pop_up_widget, 'sumlists_close_ok',
                lambda action: self.sum_lists_close_ok())
        urwid.connect_signal(self._pop_up_widget, 'close_cancel',
                lambda action: self.close_pop_up())
        (self.pop_w,self.pop_h) = (40,12)
        self.open_pop_up()
        # }}}
    def sum_lists_close_ok(self): # {{{
        global current_stash
        global current_list
        global displ
        current_stash.lists[current_list].sumlists = [ l._text for l in self._pop_up_widget._focus_list if l._checked ]
        #pinfo(str(current_stash.lists[current_list].sumlists))
        displ.w_backpack.body = displ.populate_backpack()
        displ.w_backpack._invalidate()
        self.close_pop_up()
        # }}}
    def open_help(self): # {{{
        self._pop_up_widget = HelpDialog()
        urwid.connect_signal(self._pop_up_widget, 'info_close',
                lambda action: self.just_close())
        (self.pop_w,self.pop_h) = (60,20)
        self.open_pop_up()
        # }}}

    def just_close(self): # {{{
        #pinfo(str(current_stash.lists[current_list].sumlists))
        self.close_pop_up()
        # }}}

# }}}

class GUIDisplay: # {{{

    palette = [
        ('body',         'black',      'light gray', 'standout'),
        ('header',       'white',      'dark red',   'bold'),
        ('headerbutton', 'light blue',      'black',   ''),
        ('button normal','light gray', 'dark blue', 'standout'),
        ('button select','white',      'dark green'),
        ('mode selected','white',      'dark green'),
        ('mode grayed','light gray',      ''),
        ('item selected','white',      'dark green'),
        ('item not selected','white',''),
        ('item not selectable','dark green',''),
        ('list checked', 'yellow', ''),
        ('category not selected','dark red', ''),
        ('category selected','white', 'dark blue'),
        ]
    def __init__(self):
        self.view = self.setup_view()
        self.mode = 0
    def setup_view(self):
        self.w_status = urwid.Text(('bright',''),'left','clip')
        self.w_list_name = urwid.AttrWrap(TextButton('list'), 'headerbutton')
        self.w_bp_name = urwid.AttrWrap(TextButton('backpack'), 'headerbutton')
        self.w_list_next = TextButton('| >>> ')
        self.w_list_prev = TextButton(' <<< |')
        self.w_bp_next = TextButton('| >>> ')
        self.w_bp_prev = TextButton(' <<< |')
        self.w_stash_name = urwid.AttrWrap(urwid.Text('The Stash','center','clip'), 'headerbutton')
        self.w_backpack = BackpackWidget(self.populate_backpack())
        self.w_stash = StashWidget(self.populate_stash())
        self.w_stash_itemdialog = StashPopUp(self.w_stash)
        self.w_listmode_view = urwid.Columns([
            (50,urwid.Frame(self.w_stash_itemdialog,header = urwid.Pile([urwid.Divider('-'),self.w_stash_name,urwid.Divider('-')]))),
            (1,urwid.SolidFill('|')),
            #urwid.Frame(self.w_backpack,header = urwid.Pile([urwid.Divider('-'),urwid.Columns([self.w_list_prev,self.w_list_name,self.w_list_next]),urwid.Columns([self.w_bp_prev,self.w_bp_name,self.w_bp_next]),urwid.Divider('-')]))
            #urwid.Frame(self.w_backpack,header = urwid.Pile([urwid.Divider('-'),self.w_list_name,urwid.Columns([('weight',1,self.w_bp_name),('pack',self.w_bp_next)]),urwid.Divider('-')]))
            urwid.Frame(self.w_backpack,header = urwid.Pile([urwid.Divider('-'),urwid.Columns([(6,self.w_list_prev),self.w_list_name,(6,self.w_list_next)]),urwid.Divider('-'),urwid.Columns([(6,self.w_bp_prev),self.w_bp_name,(6,self.w_bp_next)]),urwid.Divider('-')]))
            ])
        #top_w = urwid.Frame(header = self.w_modebar, body = self.w_listmode_view)
        main_w = urwid.Frame(body = self.w_listmode_view)
        ## Frame
        hdr = urwid.Text("Equipy editor",'center')
        hdr = urwid.AttrWrap(hdr, 'header')
        w = urwid.Frame(header=hdr, body=main_w, footer = self.w_status)

        urwid.connect_signal(self.w_list_name._original_widget, 'click', lambda action: self.w_stash._emit('chooselist'))
        urwid.connect_signal(self.w_list_next, 'click', lambda action: next_list(1))
        urwid.connect_signal(self.w_list_prev, 'click', lambda action: next_list(-1))
        urwid.connect_signal(self.w_bp_next, 'click', lambda action: next_backpack(1))
        urwid.connect_signal(self.w_bp_prev, 'click', lambda action: next_backpack(-1))
        return w
#    def populate_stash(self):
#        global current_stash
#        items = []
#        cats = current_stash.categories()
#        cats.sort()
#        for cat in cats:
#            items.extend( [StashCatWidget(cat)] )
#            cat_items = current_stash.get_items(cat) 
#            cat_items.sort(key=lambda i: i.name)
#            items.extend( [StashItemWidget(i.name,i.uuid,i.color) for i in cat_items ] )
#        return urwid.SimpleFocusListWalker(items)
    def populate_stash(self):
        global current_stash
        # no root node, tree is a forest
        tree = [] #(StashCatWidget('Stash'), [])
        # define some children
        cats = current_stash.categories()
        cats.sort()
        for cat in cats:
            cat_items = current_stash.get_items(cat) 
            cat_items.sort(key=lambda i: i.name)
            cat_subtree = (StashCatWidget(cat), [])
            for i in cat_items:
                leaf = (StashItemWidget(i.name,i.uuid,i.color), None)
                cat_subtree[1].append(leaf)
            tree.append(cat_subtree)
        return urwidtrees.CollapsibleTree(tree)
    def populate_backpack(self):
        global current_stash
        global current_list
        global current_backpack

        if current_list is None:
            self.w_list_name.set_label("No List Available")
            self.w_bp_name.set_label("---")
            self.w_list_name.original_widget._invalidate()
            self.w_bp_name.original_widget._invalidate()
            return urwid.SimpleFocusListWalker([StashCatWidget("Empty")])

        current_stash.lists[current_list].calculate()
        ls = current_stash.lists[current_list].calclist
        calc_bp = ls.get_bp(current_backpack)
        #bp = current_stash.get_bp(current_list, current_backpack, calculated=True)
        #bp = current_stash.get_calc_bp(current_list, current_backpack)

        items = []
        if calc_bp is not None:
            self.w_list_name.set_label(current_list + ' (' +str(current_stash.lists[current_list].n_bps()) + ')')
            self.w_list_name.original_widget._invalidate()
            self.w_bp_name.set_label(calc_bp.name + ' -:- ' + str(int(calc_bp.weight())) + ' g')
            self.w_bp_name.original_widget._invalidate()
            lists_in_bp = [l for l in list(current_stash.lists.values()) if (l.has_bp(current_backpack) and 
                                                                             l.name != current_list and
                                                                             l.name in current_stash.lists[current_list].sumlists)]

            cats = current_stash.categories(calc_bp)
            cats.sort()
            #bp = current_stash.get_bp(current_list, current_backpack, calculated=True)
            if len(cats)>0:
                for cat in cats:
                    cat_text = cat + ' ' + str(int(calc_bp.weight(cat,where='all'))) + ' g'
                    items.extend( [BackpackCatWidget(cat_text)] )
                    
                    #Add own items coming from current list
                    bp = current_stash.lists[current_list].get_bp(current_backpack)
                    cat_items_own = current_stash.get_items(cat, bp, where = 'own') 
                    cat_items_own.sort(key=lambda i: -i.weight)
                    items.extend( [BackpackItemWidget(i.uuid, bp.own_items[i.uuid])  for i in cat_items_own ] )
                    #Add own items coming from other lists
                    for l in lists_in_bp:
                        l_bp = l.get_bp(current_backpack)
                        cat_items_own = current_stash.get_items(cat, l_bp, where = 'own') 
                        cat_items_own.sort(key=lambda i: -i.weight)
                        items.extend( [BackpackItemWidget(i.uuid, 
                                                          l_bp.own_items[i.uuid], 
                                                          selectable=True, 
                                                          from_list = l.name)  for i in cat_items_own ] )

                    #Add more items coming from subdivision of common items (no source list tracking)
                    cat_items_more = current_stash.get_items(cat, calc_bp, where = 'more') 
                    cat_items_more.sort(key=lambda i: -i.weight)
                    #if len(cat_items_more) > 0:
                    items.extend( [BackpackItemWidget(i.uuid, calc_bp.more_items[i.uuid], selectable=True) for i in cat_items_more ] )
            else:
                items = [StashCatWidget("Empty")]
        return urwid.SimpleFocusListWalker(items)
    def main(self):
        #configure urwid main loop
        self.loop = urwid.MainLoop(self.view, 
                                    self.palette,
                                    unhandled_input=self.unhandled_input,
                                    pop_ups=True)
        #start urwid main loop
        self.loop.run()

    def unhandled_input(self, key):
        #if key == 'f8':
        if key == 'q':
            #exit program
            raise urwid.ExitMainLoop()
        if key == '?':
            self.w_stash._emit('help')
        if key == 's':
            self.w_stash._emit('savestash')
        if key == 'L':
            self.w_stash._emit('newlist')
        if key == 'B':
            self.w_stash._emit('newbackpack')
        if key == 'S':
            self.w_stash._emit('sumlists')
        if key == 'G':
            self.w_stash._emit('chooselist')
        if key == 'r':
            self.w_stash._emit('removebackpack')
        if key == 'R':
            self.w_stash._emit('removelist')
        if key == 'T':
            current_stash.lists[current_list].calculate()
            ls = current_stash.lists[current_list].calclist
            textool = texifier.Texifier(current_stash.lists[current_list].calclist)
            tex_file = './texlist.tex'
            output_file = './texlist.pdf'
            f = open(tex_file,'w')
            f.write(textool.texify_list())
            f.close()
            FNULL = open(os.devnull, 'w')
            if platform.system() == 'Darwin':
                call(['xelatex',tex_file], stdout = FNULL)
                call(['open',output_file])	#Mac OS
            elif platform.system() == 'Linux':
                call(["pdflatex",tex_file])
                call(["xdg-open",output_file])	#Linux/X
            elif platform.system() == 'Windows':
                call(["pdflatex",tex_file])
                os.startfile(output_file)	#Windows
        if key == 'tab':
            if current_list is not None:
                next_backpack(+1)
        if key == 'shift tab':
            if current_list is not None:
                next_backpack(-1)
        if key == 'n':
            if current_list is not None:
                next_list(+1)
        if key == 'b':
            if current_list is not None:
                next_list(-1)
# }}}

def main():
#set up parser for the program
    #parser = argparse.ArgumentParser(description="LDR serial")
    #parser.add_argument('--port', dest='port', required=True)
    #args = parser.parse_args()
#initialise serial communication
#initialise urwid interface

    global current_stash
    global displ
    load_stash()

    #cl = current_stash.lists["Lungo"]
    #cl.calculate()
    #print(cl.calclist.backpacks["Lollo"].more_items)
    #exit()
    displ = GUIDisplay()

#start urwid main loop
    displ.main()

def pinfo(text):
    global displ
    displ.w_status.set_text(str(text))
    displ.w_status._invalidate()
    #plog(text)

def plog(text):
    fd = open('./log.txt', 'a')
    fd.write(text + '\n')
    fd.close()

def next_list(inc = 1):
    global current_stash
    global current_list
    global current_backpack
    global displ
    lists = sorted(list(current_stash.lists))
    sorted_lists = list(enumerate(lists))
    sorted_lists = {v:k for (k, v) in sorted_lists}
    ls_idx = sorted_lists[current_list]+inc
    if ls_idx >= len(lists):
        current_list = lists[0]
    elif ls_idx < 0:
        current_list = lists[-1]
    else:
        current_list = lists[ls_idx]
    if current_backpack not in current_stash.lists[current_list].backpacks.keys():
        current_backpack = 'common_items'

    displ.w_backpack.body = displ.populate_backpack()
    pos = list(displ.w_backpack.body.positions())
    if len(pos)>1:
        displ.w_backpack.set_focus(pos[1])
    else:
        displ.w_backpack.set_focus(pos[0])
    displ.w_backpack._invalidate()

def next_backpack(inc = 1):
    global current_stash
    global current_list
    global current_backpack
    global displ
    bp_list = sorted(list(current_stash.lists[current_list].backpacks.keys()))
    if len(bp_list) == 0:
        return

    sorted_backpacks = list(enumerate(bp_list))
    sorted_backpacks = {v:k for (k, v) in sorted_backpacks} 

    if current_backpack == 'common_items':
        if inc == 1:
            current_backpack = bp_list[0]
        elif inc == -1:
            current_backpack = bp_list[-1]
    else:
        bp_idx = sorted_backpacks[current_backpack]+inc
        if bp_idx >= len(bp_list) or bp_idx < 0:
            current_backpack = 'common_items'
        else:
            current_backpack = bp_list[bp_idx]

    displ.w_backpack.body = displ.populate_backpack()
    pos = list(displ.w_backpack.body.positions())
    if len(pos)>1:
        displ.w_backpack.set_focus(pos[1])
    else:
        displ.w_backpack.set_focus(pos[0])
    displ.w_backpack._invalidate()
#except IndexError:
#    current_backpack = 'common_items'

def load_stash(stash_file = 'stash.json'):
    global current_stash 
    global current_list
    global current_backpack
    current_stash = stash.stash()
    current_stash.load(stash_file)
    current_list = list(current_stash.lists.keys())[0]
    current_backpack = 'common_items'
        
def save_stash(stash_file = 'stash.json'):
    global current_stash 
    global current_list
    global current_backpack
    current_stash.save(stash_file)
    
if '__main__'==__name__:
    main()
