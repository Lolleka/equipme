import urwid
import urwidtrees

class NoButtonsDialog(urwid.WidgetWrap): # {{{
    signals = ['close_dialog']
    def __init__(self, title = "Dialog", w_additional = []):
        self.w_title = urwid.Text(title, align='center')
        self.w_pile = urwid.Filler(urwid.Pile([
                                self.w_title,
                                urwid.Divider("-")]
                                + w_additional),valign='top')
        super(NoButtonsDialog, self).__init__(urwid.LineBox(self.w_pile))
    def keypress(self, size, key):
        self.w_pile.keypress(size,key)
        if key == 'esc':
            self._emit('close_dialog')
        return key
    def mouse_event(self, size, event, button, col, row, focus):
        if event == 'mouse press':
            if button == 4: #scroll up
                r = self.keypress(size, 'up')
            if button == 5: #scroll down
                r = self.keypress(size, 'down')
# }}}

class YesNoDialog(urwid.WidgetWrap): # {{{
    signals = ['yesno_close_yes','yesno_close_no']
    def __init__(self, title = "Confirm Action"):
        self.w_title = urwid.Text(title, align='center')
        self.w_yes = urwid.Button("Yes")
        self.w_no = urwid.Button("No")
        self.w_pile = urwid.Filler(urwid.Pile([
                                self.w_title,
                                urwid.Divider("-"),
                                urwid.Columns([('weight',1,self.w_yes),('weight',1,self.w_no)])
                                ]),valign='top')
        urwid.connect_signal(self.w_yes, 'click', lambda action: self._emit('yesno_close_yes'))
        urwid.connect_signal(self.w_no, 'click', lambda action: self._emit('yesno_close_no'))
        super(YesNoDialog, self).__init__(urwid.LineBox(self.w_pile))
    def keypress(self, size, key):
        self.w_pile.keypress(size,key)
        if key == 'esc':
            self._emit('yesno_close_no')
        #return key
# }}}

class OkCancelDialog(urwid.WidgetWrap): # {{{
    signals = ['close_ok', 'close_cancel']
    def __init__(self, title = "Dialog", w_additional = []):
        self.w_title = urwid.Text(title, align='center')
        self.w_ok_button = urwid.Button("Ok")
        self.w_cancel_button = urwid.Button("Cancel")
        self.w_list = urwid.ListBox(urwid.SimpleFocusListWalker([
                                self.w_title,
                                urwid.Divider("-")] +
                                w_additional +
                                [self.w_ok_button,
                                self.w_cancel_button]))
        urwid.connect_signal(self.w_cancel_button, 'click', lambda action: self._emit('close_cancel'))
        urwid.connect_signal(self.w_ok_button, 'click', lambda action: self._emit('close_ok'))
        super(OkCancelDialog, self).__init__(urwid.LineBox(self.w_list))
    def keypress(self, size, key):
        self.w_list.keypress(size,key)
        if key == 'enter':
            try:
                next_pos = self.w_list.body.next_position(self.w_list.focus_position)
                self.w_list.set_focus(next_pos)
                self.w_list._invalidate()
            except IndexError:
                pass
        elif key == 'esc':
            self._emit('close_cancel')
        #return key
# }}}

class NewStashItemDialog(OkCancelDialog): # {{{
    #signals = ['newitem_close_ok','edititem_close_ok','close_cancel']
    def __init__(self):
        #self.signals += ['newitem_close_ok','edititem_close_ok']
        self.w_name = urwid.Edit(caption = "Name: ", edit_text = "")
        self.w_category = urwid.Edit(caption = "Category: ", edit_text = "general")
        self.w_weight = urwid.IntEdit(caption = "Weight (g): ")
        self.w_description = urwid.Edit(caption = "Description: ", edit_text = "")
        self.w_color = urwid.Edit(caption = "Color: ", edit_text = "")
        super(NewStashItemDialog, self).__init__('Add New Item',[self.w_name, self.w_category, self.w_weight, self.w_description, self.w_color])
# }}}

class ColorList(urwid.WidgetWrap):
    def __init__(self):
        colors = ['light red', 'light blue', 'light green']
        _w_colorcolumns = urwid.Columns([urwid.AttrWrap(urwid.Text('O', wrap='any'),c) for c in colors]);
        super(ColorList,self).__init__(_w_colorcolumns)

class EditStashItemDialog(OkCancelDialog): # {{{
    #signals = ['newitem_close_ok','edititem_close_ok','close_cancel']
    def __init__(self):
        #self.signals += ['newitem_close_ok','edititem_close_ok']
        self.w_name = urwid.Edit(caption = "Name: ", edit_text = "")
        self.w_category = urwid.Edit(caption = "Category: ", edit_text = "general")
        self.w_weight = urwid.IntEdit(caption = "Weight (g): ")
        self.w_description = urwid.Edit(caption = "Description: ", edit_text = "")
        self.w_color = urwid.Edit(caption = "Color: ", edit_text = "")
        #self.w_color = ColorList
        super(EditStashItemDialog, self).__init__('Edit Item', [self.w_name, self.w_category, self.w_weight, self.w_description, self.w_color])
# }}}

class NewListDialog(OkCancelDialog): # {{{
    signals = ['newlist_close_ok','close_cancel']
    def __init__(self, edit = False):
        self.w_name = urwid.Edit(caption = "Name: ", edit_text = "")
        super(NewListDialog, self).__init__('Add New List', [self.w_name])
        urwid.connect_signal(self.w_ok_button, 'click', lambda action: self._emit('newlist_close_ok'))
# }}}

class NewBackpackDialog(OkCancelDialog): # {{{
    signals = ['newbackpack_close_ok','close_cancel']
    def __init__(self, edit = False):
        self.w_name = urwid.Edit(caption = "Name: ", edit_text = "")
        super(NewBackpackDialog, self).__init__('Add New Backpack', [self.w_name])
        urwid.connect_signal(self.w_ok_button, 'click', lambda action: self._emit('newbackpack_close_ok'))
# }}}

class HighlightableItem(urwid.Widget): # {{{

    _selectable = True
    _checkable = False
    _checked = False
    _sizing = frozenset('box')
    _text = 'empty'
    _color = 'default'
    _align = 'left'
    def __init__(self, text, color = 'default', checkable = False, align = 'left'):
        self._text = text
        self._color = color
        self._checkable = checkable
        self._align = align
    def rows(self, size, focus = False):
        return 1
    def render(self, size, focus = False):
        if self._checked and not focus:
            new_attr = 'list checked'
        elif self._color == 'default' or self._color == '' or self._color is None:
            new_attr = 'item not selected'
        else:
            try:
                new_attr = urwid.AttrSpec(self._color, 'default')
            except urwid.display_common.AttrSpecError:
                new_attr = 'item not selected'
                pass
        return urwid.AttrMap(urwid.Text(self._text,wrap='any', align=self._align), new_attr, focus_map='item selected').render(size, focus = focus)
    def keypress(self, size, key):
        if self._checkable and key == ' ':
            self._checked = not self._checked
        else:
            return key
# }}}

class SelText(urwid.Text): # {{{
    _selectable = True
    def keypress(self, size, key):
        return key
# }}}

class InfoDialog(urwid.WidgetWrap): # {{{
    signals = ['info_close']
    def __init__(self, title = "Info", text = "Info text"):
        self.w_title = urwid.Text(title, align='center')
        text_lines = text.split("\n")
        #pinfo(len(text_lines))
        #text_list = [ lambda l: urwid.Text(l, align='center') for l in text_lines ]
        text_list = []
        for l in text_lines:
            text_list = text_list + [HighlightableItem(l, color='light blue', align='center')]
        self.w_list = urwid.ListBox(urwid.SimpleFocusListWalker(text_list))
        w_frame = urwid.Frame(self.w_list, header = urwid.Pile([self.w_title, urwid.Divider("-")]))
        super(InfoDialog, self).__init__(urwid.LineBox(w_frame))
    def keypress(self, size, key):
        if key == "up" or key == "down":
            self.w_list.keypress(size,key)
        #if key == 'esc' or key == 'enter':
        else:
            self._emit('info_close')
        return key
    def mouse_event(self, size, event, button, col, row, focus):
        if event == 'mouse press':
            if button == 4: #scroll up
                r = self.keypress(size, 'up')
            if button == 5: #scroll down
                r = self.keypress(size, 'down')
# }}}

class FocusableText(urwid.WidgetWrap): # {{{
    """Selectable Text used for nodes in our example"""
    def __init__(self, txt):
        t = urwid.Text(txt)
        w = urwid.AttrMap(t, 'body', 'focus')
        urwid.WidgetWrap.__init__(self, w)

    def selectable(self):
        return True

    def keypress(self, size, key):
        return key

# }}}

class TextButton(urwid.Button): # {{{
    """Selectable Text used for nodes in our example"""
    def rows(self, size, focus=False):
        return 1
    def render(self, size, focus=False):
        return urwid.Text(self.label, 'center', 'clip').render(size,focus)
# }}}

