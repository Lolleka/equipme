#!/usr/bin/env python3
#this script makes use of numpy and xlrd packages. Be sure they are properly installed in your python framework.
from numpy import *
from xlrd import *
from subprocess import call
import os, platform, sys
sys.path.append('.')

from equipy import *
#from listifier import *
from tabulate import tabulate
import argparse


parser = argparse.ArgumentParser()
parser.add_argument('-x', '--excel_file', help='Select which excel file to use',required=False, default='trekking_list.xlsx')
parser.add_argument('-n', '--nocompile', action='store_true', help='Do not compile and open the output tex file', default=False)
parser.add_argument('-i', '--items', action='store_true', help='Print out items list', default=False)
parser.add_argument('-o', '--output', nargs=1, default=['Lista.pdf'], help='Overwrite default output file')
parser.add_argument('-l', '--targetlist', nargs='*', help='List set to process (in given order)',default=[])
parser.add_argument('-s', '--sumlists', action='store_true', help='Sums list to produce an aggregated result',default=False)
parser.add_argument('-w', '--worksheet', nargs=1, default=['Materiale'], help='Select which worksheet to use') 
group = parser.add_mutually_exclusive_group()
group.add_argument('-r', '--report', action='store_true', help='Output report to terminal', default=False)
group.add_argument('-t', '--tex_report', action='store_true', help='Output tex file to terminali', default=False)

args = parser.parse_args()
excel_file = args.excel_file
worksheet_name = args.worksheet[0] 
target_lists = args.targetlist
report = args.report
tex_report = args.tex_report
nocompile = args.nocompile
output_file = args.output[0]
print_items = args.items

print('Processing %s ...\nWorksheet: %s\nTarget lists: %s\n\n' % (excel_file, worksheet_name, target_lists))

#Opens workbook (trekking list)
workbook = open_workbook(excel_file)
#Opens worksheet within the workbook
worksheet = workbook.sheet_by_name(worksheet_name)

#Auxiliary variables
n_rows = worksheet.nrows - 1
n_cols = worksheet.ncols - 1

weight = array([])  #Contains the weight for each item
common = array([])  #Contains the common quantity for each item

#Fills in an auxiliary matrix, bigmatrix, for easiness of elaboration
bigmatrix = array([]).reshape(0, n_cols+1)
for r in range(1, n_rows+1):
    if not worksheet.cell_type(r,0) == 0:	#Skip rows that have empty cells on the first column.
        bigmatrix = r_[bigmatrix, [asarray(worksheet.row_values(r))]]

#Saves the first row of the worksheet, because it contains the party members names
header=asarray(worksheet.row_values(0))
#Delete columns corresponding to empty strings (i.e. columns without a label)
todelete = argwhere(header=='')
header = delete(header, todelete.T[0], None)
bigmatrix = delete(bigmatrix,todelete.T[0],axis=1)

header_lists = header[3:]

#Converts empty strings to '0.0'
bigmatrix[bigmatrix=='']='0.0'
#Sorts matrix by weight
sorted_index=bigmatrix[:,2].astype(float).argsort()[::-1]
bigmatrix = bigmatrix[sorted_index,:]

#Assign the matrix columns a new mnemonic name
categories=list(bigmatrix[:,0]) #Containes the ordered category list
item_names=list(bigmatrix[:,1]) #Contains the item string list
weights=list(bigmatrix[:,2].astype(float))	#Contains the weight for each item

list_collection = {}
#Build stash
s = stash.stash()
for (i,name) in enumerate(item_names):
    new_item = stash.gearitem(name, categories[i],weights[i])
    item_names[i] = new_item.uuid
    s.add_item(new_item)

#build lists
for (i,col_name) in enumerate(header_lists):
    if (':' in col_name):
        
        (list_name, bp_name) = str.split(str(col_name), ':')
        if list_name not in s.lists.keys():
            s.lists[list_name] = equiplist.equiplist(list_name, stash=s)
        ls = s.lists[list_name]
        ls.add_bp(bp_name)
        bp = ls.backpacks[bp_name]

        geardata = zip(item_names, categories, weights, list(bigmatrix[:,i+3].astype(float).astype(int)))

        for (item_uuid, cat, w, q) in list(geardata):
            if q > 0:
                ls.add_item(bp_name,item_uuid, q)

    else:
        list_name = col_name
        if list_name not in s.lists.keys():
            s.lists[list_name] = equiplist.equiplist(list_name, stash=s)
        ls = s.lists[list_name]

        geardata = zip(item_names, categories, weights, list(bigmatrix[:,i+3].astype(float).astype(int)))
        for (item_uuid, cat, w, q) in list(geardata):
            if q > 0:
                ls.add_item('common_items',item_uuid, quantity=q)

print (s.json_str())
exit(0)
list_collection_keys = list(list_collection.keys())
if len(target_lists) > 0:
    for l in list_collection_keys:
        if l not in target_lists:
            del list_collection[l] 

if len(list_collection) == 0:
    exit(1)


