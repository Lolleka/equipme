
import numpy as np
import os
import copy
import math
import json
from tabulate import tabulate
from . import equiplist
from . import backpack
#from gearlist import gearitem, memberstat


class Texifier():
    _l = None
    def __init__(self, l):
        self._l = l
    def texify_list(self, l = None):
        if l is None:
            if self._l is None:
                return ""
            else:
                l = self._l
        self._l = l

        flag = 0
        rep_str = '\\documentclass[]{article}\n'
        rep_str += '\\usepackage{amsmath}\n'
        rep_str += '\\usepackage[a4paper, portrait, margin=2cm]{geometry}\n'
        rep_str += '\\usepackage{amssymb}\n'
        rep_str += '\\usepackage{array}\n'
        rep_str += '\\usepackage{graphicx}\n'
        rep_str += '\\usepackage{url}\n'
        rep_str += '\\usepackage{multirow}\n'
        rep_str += '\\usepackage{epstopdf}\n'
        rep_str += '\\usepackage{bm}\n'
        rep_str += '\\usepackage{color}\n'
        rep_str += '\\usepackage{hyperref}\n'
        rep_str += '\\usepackage{colortbl}\n'
        rep_str += '\\usepackage{xspace}\n'
        rep_str += '\\usepackage{pstricks}\n'
        rep_str += '\\usepackage{pstricks-add}\n'
        rep_str += '\\usepackage[default,osfigures,scale=0.95]{opensans}'
        rep_str += '\\usepackage[italian]{babel}\n'
        rep_str += '\\usepackage[T1]{fontenc}\n'
        rep_str += '\\usepackage[utf8]{inputenc} \n'
        rep_str += '\\definecolor{Gray}{gray}{0.9}\n'
        rep_str += '\\definecolor{White}{gray}{1}\n'
        rep_str += '\\definecolor{Yellow}{rgb}{1,1,0.6}\n'
        rep_str += '\\definecolor{LightCyan}{rgb}{0.88,1,1}\n'
        rep_str += '\\newcommand{\\ghline}{\\arrayrulecolor{Gray}\\hline\\arrayrulecolor{black}}\n'
        rep_str += '\\renewcommand\\thesection{}\n'
        rep_str += '\\renewcommand\\thesubsection{}\n'
        #rep_str += '\\newcolumntype{L}{>{\centering\arraybackslash}m{3cm}}'
        rep_str += '\\title{Lista trekking}\n'
        rep_str += '\\begin{document}\n'
        rep_str += '\\centering\n'
        rep_str += '\\section{%s}\n\n' % str(l.name)
        rep_str += '\\begin{Form}\n\n'
        if l.list_type == "Party":
            for bp in l.backpacks.values():	#Create a first table with statistics for each party member
                rep_str += self.texify_backpack(bp)
                rep_str += '\\newpage\n'
#End tex document and close file
        else:
            rep_str += self.texify_backpack(l.common_items)
        rep_str += '\\end{Form}\n\n'
        rep_str += '\\end{document}\n'

        return rep_str
    def texify_backpack(self, bp):
        categories = bp.categories()
        categories.sort()  # sorts in place
        tot_weight = bp.total_weight()
        rep_str = '\\subsection{%s (Totale: %2.1f Kg)}\n\n' % (bp.name, tot_weight/1e3)

        rep_str += '\\small\n'
        rep_str += '\t\\begin{tabular}{p{5cm}>{\\raggedleft\\arraybackslash}cr}\n'
        rep_str += '\t\t {\\normalsize \\textbf{Category}} & weight & \\% \\\\\n'
        rep_str += '\t\t\\hline\n'
        count = 0
        for category in categories:
            cat_weight = bp.weight(category) 
            if count > 0:
                rep_str += '\t\t\\ghline\n'
            rep_str += '\t\t %s & %u g & %u \\\\\n' % (category, cat_weight, math.ceil(cat_weight*100/tot_weight))
            count += 1
        rep_str += '\t\t\\hline\n'
        rep_str += '\t\t & %u g & \\\\\n' % (tot_weight)
        rep_str += '\t\\end{tabular}\n\n\\vspace{1em}'

        for category in categories:
            cat_weight = bp.weight(category) 
            rep_str += '\\small\n'
            rep_str += '\t\\begin{tabular}{p{5cm}>{\\raggedleft\\arraybackslash}p{6cm}>{\\raggedleft\\arraybackslash}p{1.5cm}>{\\raggedright\\arraybackslash}cr}\n'
            rep_str += '\t\t {\\normalsize \\textbf{%s}} & description & weight & qty & \\\\\n' % (category)
            rep_str += '\t\t\\hline\n'
            count = 0 
            #flag = 0	#Row color flag
            for i in [i for i in self._l.stash.get_items(category, bp,'all')]:
            #    if flag:
            #            rep_str += '\t\t\\rowcolor{Yellow}\n'
            #    flag = 1 - flag
                #rep_str += '\t\t%ux & %s & %u g & \\mbox{\\CheckBox[width=0.4cm,name=foo%u]{\\vphantom{!}}}\\\\\n' % (i.quantity, i.name, i.weight*i.quantity,count)
                if count > 0:
                    rep_str += '\t\t\\ghline\n'
                qty = bp.tot_item_qty(i.uuid)
                rep_str += '\t\t %s & %s & %u g & %u & $\\Box$ \\\\\n' % (i.name, i.description, i.weight*qty, qty)
                count += 1
            #rep_str += '\t\t\\rowcolor{LightCyan}\n'
            rep_str += '\t\t\\hline\n'
            rep_str += '\t\t & & %u g & & \\\\\n' % (cat_weight)
            rep_str += '\t\\end{tabular}\n\n\\vspace{1em}'
        #Wrap the current item list up. The row for the subtotal count is highlighted in cyan.
        #rep_str += '\\newpage\n'
        return rep_str
    def tex_pie(self):
        perc = self.percentages()
        (perc, categories) = (perc.values(), perc.keys())
        rep_str = '\t\\begin{figure}[h]\n'
        rep_str += '\t\\centering\n'
        rep_str += '\t\\begin{pspicture}(-5.5,-5.5)(5.5,5.5)\n'
        rep_str += '\t\\psset{unit=1.2}\n'
        ncolors = len(categories)
        colors = ['red!30','green!30','blue!40','gray','magenta!60','cyan'] * math.ceil(float(ncolors)/6)
        rep_str += '\t\\psChart[userColor={%s}, chartNodeO=1.5, chartNodeI=0.7]{' % ','.join(colors)
        rep_str += ','.join(map('{:.2f}'.format, perc))
        rep_str += '}{}{3}\n'
        rep_str += '\t\\bfseries\n'
        rep_str += '\t\\small\n'
        i = 1
        for category in categories:
            cat_weight = self._calc_subset_weight([i for i in self.own_items + self.more_items if i.category == category])
            rep_str += '\t\\ncline[arrows=-, arrowscale=1.5, nodesepB=2pt]{psChart%d}{psChartO%d}\n' % (i,i)
            rep_str += '\t\\rput[l](psChartO%d){%s}\n' % (i,category)
            rep_str += '\t\\rput(psChartI%d){%d g}\n' % (i,cat_weight)
            #rep_str += '\t\\rput(psChartI%d){%.1f \\%%}\n' % (i,self.percentages()[category])
            i += 1
        rep_str += '\t\\end{pspicture}\n\n'
        rep_str += '\t\\end{figure}\n\n'
        return rep_str
