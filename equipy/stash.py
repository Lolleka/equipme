import numpy as np
import os
import copy
import math
import uuid
import json
from tabulate import tabulate
from . import equiplist

#A simple structure to hold party member data.
class gearitem:
    def __hash__(self):
        return hash(tuple(self))
    def __init__(self,name,category = 'general', weight = 0, description = '', color = None, quantity=1,uuid=-1):
        self.name = name
        self.category = category
        self.weight = weight
        self.quantity = quantity
        self.description = description
        self.color = color
        if uuid == -1: 
            self.uuid = self._gen_uuid()
        else:
            self.uuid = uuid
    def __str__(self):
        return str(self.quantity) + "x " +  self.name + " (" + self.category + ", " + str(self.weight) + "g)"
    def __eq__(self,other):
        return (self.name == other.name and self.category == other.category and self.weight == other.weight)
    def _gen_uuid(self):
        self.uuid = uuid.uuid4().fields[-1]
        return self.uuid
    @classmethod
    def from_gearitem(cls, orig):
        return cls(orig.name, category=orig.category, weight=orig.weight, description=orig.description, color=orig.color, quantity=orig.quantity )
    def json_str(self):
        tmp_dict = {"name": self.name, "uuid":str(self.uuid), "weight":self.weight, "category":self.category, "description":self.description}
        if self.color is not None:
            tmp_dict["color"] = str(self.color)
        return json.dumps(tmp_dict)

class stash:
    def __init__(self):
        self.name = ""
        self.items = {}
        self.lists = {}
    def add_item(self, item):
        self.items[item.uuid] = item
    def remove_item(self, item_uuid):
        # item can be an item reference or a unique id
        del self.items[item_uuid]
        for ls in self.lists.values():
            ls.remove_item(None, item_uuid)
    def add_to_bp(self, list_name = None, bp_name = None, item_uuid = None, quantity=1, where = 'own'):
        if list_name in self.lists.keys():
            self.lists[list_name].add_item(bp_name, item_uuid, quantity, where)

    def add_list(self,list_name, json_data = None):
        # item can be an item reference or a unique id
        if not list_name in self.lists.keys():
            self.lists[list_name] = equiplist.equiplist(list_name, json_data, self)
    def remove_list(self,list_name):
        # item can be an item reference or a unique id
        if list_name in self.lists.keys():
            del self.lists[list_name]
    def load(self, stash_file):
        #load and parse json file
        with open(stash_file) as json_data:
            data = json.load(json_data)
        self.name = data['name']
        #self.items = [None]*len(data['items'])
        for i in data['items']:
            self.items[i['uuid']] = gearitem(i['name'],i['category'],i['weight'],uuid=i['uuid'], description=i['description']) 
            if 'color' in i.keys():
                self.items[i['uuid']].color = i['color']
        json_lists = data['lists']
        for l in data['lists']:
            self.add_list(l['name'], l)
        #    self.own_items[i] = data['items'][i]
        return 0
    def save(self, stash_file):
        with open(stash_file, "w") as f:
            f.write(self.json_str())
        f.close()
    def json_str(self):
        ret_str =  ''
        item_list = [json.loads(i.json_str()) for i in self.items.values()]
        list_list = [json.loads(ls.json_str()) for ls in self.lists.values()]
        return json.dumps({"name":self.name, "items":item_list,"lists":list_list}, indent=4)

    #def categories(self, list_name = None, bp_name = None):
    def categories(self, bp = None, where = 'all'):
        if bp is None:
            return list(set([i.category for i in self.items.values()]))
        else:
            own_items_keys = list(bp.own_items.keys()) 
            more_items_keys = list(bp.more_items.keys())
            if where == 'all':
                items_keys = list(bp.own_items.keys()) + list(bp.more_items.keys())
            elif where == 'own':
                items_keys = list(bp.own_items.keys())
            elif where == 'more':
                items_keys = list(bp.more_items.keys())
            bp_items = [ self.items[k] for k in items_keys ]
            return list(set([i.category for i in bp_items]))
    def get_items(self, category = None, bp = None, where='all'):
        if bp is None:
            return [i for i in self.items.values() if i.category == category]
        else:
            own_items_keys = list(bp.own_items.keys()) 
            more_items_keys = list(bp.more_items.keys())
            if where == 'all':
                items_keys = own_items_keys + more_items_keys
            elif where == 'own':
                items_keys = own_items_keys
            elif where == 'more':
                items_keys = more_items_keys
            bp_items = [ self.items[k] for k in items_keys ]
            if category is None:
                return bp_items
            else:
                return [i for i in bp_items if i.category == category]
    def get_bp(self, list_name, bp_name, calculated = False):
        if list_name in self.lists.keys():
            if calculated:
                return  self.lists[list_name].calclist.get_bp(bp_name)
            else:
                return  self.lists[list_name].get_bp(bp_name)
    def remove_from_bp(self, list_name = None, bp_name = None, item_uuid = None, quantity=1, where = 'own'):
        if list_name in self.lists.keys():
            self.lists[list_name].remove_item(bp_name, item_uuid, quantity, where)
    def add_to_bp(self, list_name = None, bp_name = None, item_uuid = None, quantity=1, where = 'own'):
        if list_name in self.lists.keys():
            self.lists[list_name].add_item(bp_name, item_uuid, quantity, where)
    def remove_bp(self,list_name, bp_name):
        # item can be an item reference or a unique id
        if list_name in self.lists.keys():
            self.lists[list_name].remove_bp(bp_name)
