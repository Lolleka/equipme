import numpy as np
import copy
import math
import json
from . import stash
from tabulate import tabulate

class backpack:
    def __init__(self, name = 'name', json_itemdata = None, stash = None, parent_list_name = ""):
        self.own_items = {}	#Items array
        self.more_items = {}	#Items array
        self.name = name        #Member name
        self.stash = stash
        self.parent_list_name = parent_list_name
        if json_itemdata is not None:
            for i in json_itemdata.keys():
                self.own_items[i] = json_itemdata[i]
    @classmethod
    def from_backpack(bp, orig):
        copy_bp = backpack(orig.name, stash = orig.stash, parent_list_name = orig.parent_list_name)
        copy_bp.own_items = orig.own_items.copy()
        copy_bp.more_items = orig.more_items.copy()
        return copy_bp
    def __add__(self, other):
        if other == 0:
            return self
        sum_bp = backpack(self.name + "+" + other.name, stash = self.stash, parent_list_name = self.parent_list_name + "+" + other.parent_list_name)
        sum_bp.own_items = self.own_items.copy()
        sum_bp.more_items = self.more_items.copy()
        for (key,value) in other.own_items.items():
            if key in sum_bp.own_items.keys():
                sum_bp.own_items[key] += value
            else:
                sum_bp.own_items[key] = value
        for (key,value) in other.more_items.items():
            if key in sum_bp.more_items.keys():
                sum_bp.more_items[key] += value
            else:
                sum_bp.more_items[key] = value
        return sum_bp
    def __radd__(self, other):
        if other == 0:
            return self
        return __add__(other)
    def __str__(self):
        return "name = " + self.name + ", own = " + str(self.own_items) + ", more = " + str(self.more_items)
    def load_json_data(self, json_data):
        data = json.loads(json_str)
        self.name = data['name']
        for i in data['items'].keys():
            self.own_items[i] = data['items'][i]
    def json_str(self):
        for uuid in self.own_items.keys():
            self.own_items[uuid] = int(self.own_items[uuid])
        return json.dumps({"name":self.name, "items":self.own_items}, indent = 4)
    def total_weight(self):
        return self.own_weight() + self.more_weight()
    def more_weight(self):
        return sum([self.stash.items[i].weight * self.more_items[i] for i in self.more_items.keys()])
    def own_weight(self):
        return sum([self.stash.items[i].weight * self.own_items[i] for i in self.own_items.keys()])
    def get_item(self,item_uuid):
        return self.stash.items[item_uuid]
    def has_item(self,item_uuid, where = 'own'):
        if where == 'own' or where == 'all':
            if item_uuid in self.own_items.keys():
                return True
            else:
                return False
        elif where == 'more' or where == 'all':
            if item_uuid in self.more_items.keys():
                return True
            else:
                return False
    def add_item(self, item_uuid, quantity = 1, where = 'own'):
        if where == 'own' or where == 'all':
            if item_uuid in self.own_items.keys():
                self.own_items[item_uuid] += quantity
            else:
                self.own_items[item_uuid] = quantity
        elif where == 'more' or where == 'all':
            if item_uuid in self.more_items.keys():
                self.more_items[item_uuid] += quantity
            else:
                self.more_items[item_uuid] = quantity
    def remove_item(self, item_uuid, quantity = None, where = 'all'):
        if where == 'own' or where == 'all':
            if item_uuid in self.own_items.keys():
                if quantity is None:
                    del self.own_items[item_uuid]
                else:
                    self.own_items[item_uuid] -= quantity
                    if self.own_items[item_uuid] <= 0:
                        del self.own_items[item_uuid]
        elif where == 'more' or where == 'all':
            if item_uuid in self.more_items.keys():
                if quantity is None:
                    del self.more_items[item_uuid]
                else:
                    self.more_items[item_uuid] -= quantity
                    if self.more_items[itwm_uuid] <= 0:
                        del self.more_items[item_uuid]
    def reset(self, where = 'all'):
        if where == 'all':
            self.own_items = {}
            self.more_items = {}
        elif where == 'own':
            self.own_items = {}
        elif where == 'more':
            self.more_items = {}
    def percentages(self):
        categories = self.categories()
        rep_dict ={}
        cat_weights = {c:(sum([self.stash.items[i].weight * self.own_items[i] for i in self.own_items.keys() if self.stash.items[i].category == c])
                         +sum([self.stash.items[i].weight * self.more_items[i] for i in self.more_items.keys() if self.stash.items[i].category == c])) for c in categories}
        for category in categories:
            cat_weight = cat_weights[category]
            rep_dict[category] = 100*cat_weight/sum(cat_weights.values())
        return rep_dict
    def weight(self, category = None, where='all'):
        if category == None:
            categories = self.categories()
            cat_weights_own = sum([sum([self.stash.items[i].weight * self.own_items[i] for i in self.own_items.keys() if self.stash.items[i].category == c]) for c in categories])
            cat_weights_more = sum([sum([self.stash.items[i].weight * self.more_items[i] for i in self.more_items.keys() if self.stash.items[i].category == c]) for c in categories])
            if where == 'all':
                return cat_weights_own + cat_weights_more
            elif where == 'own':
                return cat_weights_own
            elif where == 'more':
                return cat_weights_more
        else:
            categories = self.categories()
            if category in categories:
                cat_weights_own = sum([self.stash.items[i].weight * self.own_items[i] for i in self.own_items.keys() if self.stash.items[i].category == category])
                cat_weights_more = sum([self.stash.items[i].weight * self.more_items[i] for i in self.more_items.keys() if self.stash.items[i].category == category])
                if where == 'all':
                    return cat_weights_own + cat_weights_more
                elif where == 'own':
                    return cat_weights_own
                elif where == 'more':
                    return cat_weights_more
        return 0
    def categories(self):
        items_keys = list(self.own_items.keys()) + list(self.more_items.keys())
        bp_items = [ self.stash.items[k] for k in items_keys ]
        return list(set([i.category for i in bp_items]))
    def tot_item_qty(self, uuid):
        qty = 0
        if self.has_item(uuid, 'own'):
            qty += self.own_items[uuid]
        if self.has_item(uuid, 'more'):
            qty += self.more_items[uuid]
        return qty


if __name__ == "__main__":
    s = stash.stash()
    i = stash.gearitem("item 1", "cat 1", 10, 1)
    i.uuid = "u1"
    s.add(i)
    i = stash.gearitem("item 2", "cat 2", 20, 1)
    i.uuid = "u2"
    s.add(i)
    i = stash.gearitem("item 3", "cat 1", 30, 1)
    i.uuid = "u3"
    s.add(i)
    b = backpack()
    b.stash = s
    b.load_json('{"name":"bp1", "items":{"u1":10, "u2":1, "u3":1}}')
    print(b.own_weight())
    print(b.categories())
    print(b.percentages())
