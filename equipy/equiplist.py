import numpy as np
import json
from . import backpack
#from gearlist import gearitem, memberstat

class equiplist:
    def __init__(self, list_name, json_data = None, stash = None):
        self.stash = stash
        self.backpacks = {}
        self.sumlists = []
        self.calclist = self
        #self.parse_geardata(geardata)
        if json_data is not None:
            self.common_items = backpack.backpack('Common Items',json_data['common_items'])
            #self.common_items = memberstat("Common items")
            self.name = json_data['name']
            self.read_json_data(json_data)
            if 'sumlists' in json_data.keys():
                self.sumlists = json_data['sumlists']
        else:
            self.name = list_name
            self.common_items = backpack.backpack('Common Items')
            self.list_type = "Standalone"
        self.common_items.stash = stash
        for bp in self.backpacks.values():
            bp.stash = stash
    def __add__(self, other):
        if other == 0:
            return self
        sum_list = equiplist(self.name + "+" + other.name ,stash = self.stash)
        sum_list.common_items = self.common_items + other.common_items
        sum_list.common_items.name = 'Common Items'
        #new_list.common_items.parent_list_name = self.name + "+" other.name

        for (bp_name, bp) in self.backpacks.items():
            sum_list.backpacks[bp_name] = backpack.backpack.from_backpack(bp)
        for (bp_name, bp) in other.backpacks.items():
            if bp_name in sum_list.backpacks.keys():
                sum_list.backpacks[bp_name] += bp
                sum_list.backpacks[bp_name].name = bp_name
            else:
                sum_list.backpacks[bp_name] = backpack.backpack.from_backpack(bp)
        if len(sum_list.backpacks) > 0:
            sum_list.list_type = "Party"
        else:
            sum_list.list_type = "Standalone"
        return sum_list
    def __radd__(self, other):
        if other == 0:
            return self
        return self.__add__(other)
    def read_json_data (self,json_data):
        if len(json_data['backpacks']) > 0:
            for bp in json_data['backpacks']:
                #self.backpacks[bp['name']] = backpack.backpack(bp['name'],bp['items'],stash=self.stash)
                self.add_bp(bp['name'],bp['items'])
            if 'sumlists' in json_data.keys():
                self.sumlists = json_data['sumlists']
            self.list_type = 'Party'
        else:
            self.list_type = 'Standalone'
    def json_str(self):
        ret_str = ''
        bps = [json.loads(bp.json_str()) for bp in self.backpacks.values()]
        #print(bps)
        #for bp in self.backpacks.values():
        #    ret_str += '\n' + bp.json_str()
        for uuid in self.common_items.own_items.keys():
            self.common_items.own_items[uuid] = int(self.common_items.own_items[uuid])
        ret_dict = {"name": self.name, "stash":self.stash.name, "common_items": self.common_items.own_items, "backpacks" : bps}
        if len(self.sumlists) > 0:
            ret_dict["sumlists"] = self.sumlists
        return json.dumps(ret_dict, indent = 4) 
        #ret_str = self.common_items.json_str() + '\n' + ret_str
        #return json.dumps(json.loads(ret_str))
        #print(ret_str)
        #a = json.loads(ret_str)
        #return ret_str
    def n_bps(self):
        return len(self.backpacks.items())
    def get_bp(self, bp_name):
        if bp_name in self.backpacks.keys():
            return self.backpacks[bp_name]
        elif bp_name == 'common_items':
            return self.common_items
        return None
    def has_bp(self, bp_name):
        if bp_name in self.backpacks.keys():
            return True
        elif bp_name == 'common_items':
            return True
        return False
    def remove_bp(self, bp_name):
        if bp_name in self.backpacks.keys():
            del self.backpacks[bp_name]
        elif bp_name == 'common_items':
            self.common_items.reset()
        if len(self.backpacks) > 0:
            self.list_type = 'Party'
        else:
            self.list_type = 'Standalone'
    def add_bp (self, bp_name, json_itemdata = None):
        if bp_name == 'common_items':
            self.common_items = backpack.backpack('Common Items',json_itemdata = json_itemdata, stash = self.stash, parent_list_name = self.name)
        elif bp_name not in self.backpacks.keys():
            self.backpacks[bp_name] = backpack.backpack(bp_name, json_itemdata = json_itemdata, stash = self.stash, parent_list_name = self.name)
        if len(self.backpacks) > 0:
            self.list_type = 'Party'
        else:
            self.list_type = 'Standalone'
    def remove_item(self, bp_name = 'common_items', item_uuid = None, quantity = None, where = 'own'):
        if bp_name is None:
            for bp in self.backpacks.values():
                bp.remove_item(item_uuid, quantity, where)
            self.common_items.remove_item(item_uuid, quantity, 'own')
        else:
            if bp_name in self.backpacks.keys():
                self.backpacks[bp_name].remove_item(item_uuid, quantity, where)
            elif bp_name == 'common_items':
                self.common_items.remove_item(item_uuid, quantity, where)
    def add_item(self, bp_name = 'common_items', item_uuid = None, quantity = 1, where = 'own'):
        if quantity > 0:
            if bp_name == 'common_items':
                self.common_items.add_item(item_uuid, quantity, where='own') 
            else:
                if bp_name in self.backpacks.keys():
                    self.backpacks[bp_name].add_item(item_uuid, quantity, where) 
    def has_item(self, bp_name = 'common_items', item_uuid = None, where = 'own'):
        if bp_name == 'common_items':
            return self.common_items.has_item(item_uuid, where) 
        else:
            if bp_name in self.backpacks.keys():
                return self.backpacks[bp_name].has_item(item_uuid, where) 
    def calculate(self):
#The following for loop takes care of distributing the common items over the party members. To do this fairly, a ledgering system is put in place, whereby on each cycle the lighter party member is assigned a new item. Then the ledger is updated for the next loop. Items are sorted in decreasing order of weight, so that it becomes increasingly easy for the algorithm to 'even out' any weight differences between all party members.
        #map(lambda bp: bp.reset('more'), self.backpacks)
#Calculate sum list:
        if len(self.sumlists) > 0:
            sum_list = [self.stash.lists[listname] for listname in self.stash.lists.keys() if listname in self.sumlists] + [self]
            self.calclist = sum(sum_list)
            #self.calclist.list_type = 'Party'
        else:
            self.calclist = self

        for bp in self.calclist.backpacks.values():
            bp.reset('more')
        if self.list_type == 'Party':
        #if self.calclist.list_type == 'Party':
            sorted_common = sorted( self.calclist.common_items.own_items.items(), key=lambda i:-self.stash.items[i[0]].weight)
            for (item_uuid, quantity) in sorted_common:
                if quantity > 0:
                    for i in range(quantity):
                        mbrs = [bp for (key,bp) in self.calclist.backpacks.items() if key in self.backpacks.keys()]
                        light_bp = mbrs[np.argmin(list(map(lambda m: m.weight(), mbrs)))]
                        light_bp.add_item(item_uuid,1,where='more')
