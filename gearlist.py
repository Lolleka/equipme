import numpy as np
import os
import copy
import math
from tabulate import tabulate
#A simple structure to hold party member data.
class gearitem:
    def __init__(self,name,category,weight=0,quantity=1):
        self.name = name
        self.category = category
        self.weight = weight
        self.quantity = quantity
    #def __init__(self,itemdata):
    #    (self.name, self.category, self.weight, self.quantity) = itemdata
    def __str__(self):
        return str(self.quantity) + "x " +  self.name + " (" + self.category + ", " + str(self.weight) + "g)"
    def __eq__(self,other):
        return (self.name == other.name and self.category == other.category and self.weight == other.weight)
    @classmethod
    def from_gearitem(cls, orig):
        return cls(orig.name, orig.category, orig.weight, orig.quantity )

class memberstat:
    def __init__(self, name = ''):
        self.name = name        #Member name
        self.own_items = []	    #Items array
        self.more_items = []	#Items array
#        self.own = array([],dtype=bool)	#Raw array of personal items
    def __str__(self):
        return "name = " + self.name + ", own = " + str(self.own_items) + ", more = " + str(self.more_items)
#    def __add__(self, other):
#        tmp_list = memberstat(self.name + ' + ' + other.name)
#        tmp_list.own_items = self.own_items + other.own_items
#        tmp_list.more_items = self.more_items + other.more_items
#        return self#.pages + other
    def total_weight(self):
        return self.own_weight() + self.more_weight()
#        return self.own_weight() + sum(map(lambda i: i.weight, self.more_items))
    def more_weight(self):
        return sum([i.weight * i.quantity for i in self.more_items])
        #return sum(map(lambda i: i.weight*i.quantity, self.own_items))
    def own_weight(self):
        return sum([i.weight * i.quantity for i in self.own_items])
        #return sum(map(lambda i: i.weight*i.quantity, self.own_items))
    def get_item(self,gi_target):
        for gi in self.more_items:
            if gi == gi_target:
                return gi
        return None
    def _calc_subset_weight(self, subset):
        return sum([i.weight * i.quantity for i in subset])
    def categories(self):
        return list(set([i.category for i in self.own_items + self.more_items] + [i.category for i in self.own_items + self.more_items]))
    def percentages(self):
        categories = self.categories()
        rep_dict ={}
        cat_weights = {c:self._calc_subset_weight([i for i in self.own_items + self.more_items if i.category == c]) for c in categories}
        for category in categories:
            cat_weight = cat_weights[category]
            rep_dict[category] = 100*cat_weight/sum(cat_weights.values())
        return rep_dict
    def report(self, print_items=False):
        rep_str = "name = " + self.name + "\n" + \
              "personal items = " + str(len(self.own_items)) + "\n" + \
              "personal items weight = " + str(self.own_weight()) + " g\n" + \
              "additional items = " + str(len(self.more_items)) + "\n" + \
              "additional items weight = " + str(self.more_weight()) + " g\n" + \
              "total weight = " + str(self.total_weight()) + " g\n"

        m_table = tabulate(self.list_report(),headers=['Category','Weight (g)','%'],tablefmt='psql')
        table_len = len(m_table.split('\n')[0])
        rep_str += '-' * table_len + '\n'
        fmt = '{:^' + str(table_len - 2) + '}'
        rep_str += '|' + fmt.format(self.name) + '|\n'
        rep_str += m_table + '\n\n'
        if print_items:
            categories = self.categories()
            categories.sort()  # sorts in place
            #cat_weights = {c:self._calc_subset_weight([i for i in self.own_items + self.more_items if i.category == c]) for c in categories}
            for category in categories:
                #cat_weight = cat_weights[category]
                #cat_weight = self._calc_subset_weight([i for i in self.own_items + self.more_items if i.category == category])
                tmp_item_list = [i for i in self.own_items + self.more_items if i.category == category]
                tmp_item_list.sort(key=lambda i: i.name)
                i_table = tabulate([[str(i.quantity)+'x', i.name, '{:d}'.format(int(i.weight))+' g'] for i in tmp_item_list], tablefmt='rst')
                table_len = len(i_table.split('\n')[0])
                #rep_str += '-' * table_len + '\n'
                fmt = '{:^' + str(table_len) + '}'
                rep_str +=  fmt.format(category) + '\n'
                rep_str += i_table + '\n\n'
        return rep_str
    def tex_report(self):

        categories = self.categories()
        categories.sort()  # sorts in place
        rep_str = '\\subsection{%s (Totale: %2.1f Kg)}\n\n' % (self.name, self.total_weight()/1e3)

        for category in categories:
            cat_weight = self._calc_subset_weight([i for i in self.own_items + self.more_items if i.category == category])

            rep_str += '\t\\begin{tabular}{|c|p{9cm}>{\\raggedleft\\arraybackslash}p{1.5cm}c|}\n'
            rep_str += '\t\t\\hline\n'
            rep_str += '\t\t\\multicolumn{4}{|c|}{%s}\\\\\n' % (category)
            rep_str += '\t\t\\hline\n'
            count = 0 
            flag = 0	#Row color flag
            for i in [i for i in self.own_items + self.more_items if i.category == category]:
                if flag:
                        rep_str += '\t\t\\rowcolor{Yellow}\n'
                flag = 1 - flag
                #rep_str += '\t\t%ux & %s & %u g & \\mbox{\\CheckBox[width=0.4cm,name=foo%u]{\\vphantom{!}}}\\\\\n' % (i.quantity, i.name, i.weight*i.quantity,count)
                rep_str += '\t\t%ux & %s & %u g & $\\Box$ \\\\\n' % (i.quantity, i.name, i.weight*i.quantity)
                count += 1
            rep_str += '\t\t\\rowcolor{LightCyan}\n'
            rep_str += '\t\t\\hline\n'
            rep_str += '\t\t& Subtotale Categoria & %u g & \\\\\n' % (cat_weight)
            rep_str += '\t\t\\hline\n'
            rep_str += '\t\\end{tabular}\n\n'
        #Wrap the current item list up. The row for the subtotal count is highlighted in cyan.
        #rep_str += '\\newpage\n'
        return rep_str
    def tex_pie(self):
        perc = self.percentages()
        (perc, categories) = (perc.values(), perc.keys())
        rep_str = '\t\\begin{figure}[h]\n'
        rep_str += '\t\\centering\n'
        rep_str += '\t\\begin{pspicture}(-5.5,-5.5)(5.5,5.5)\n'
        rep_str += '\t\\psset{unit=1.2}\n'
        ncolors = len(categories)
        colors = ['red!30','green!30','blue!40','gray','magenta!60','cyan'] * math.ceil(float(ncolors)/6)
        rep_str += '\t\\psChart[userColor={%s}, chartNodeO=1.5, chartNodeI=0.7]{' % ','.join(colors)
        rep_str += ','.join(map('{:.2f}'.format, perc))
        rep_str += '}{}{3}\n'
        rep_str += '\t\\bfseries\n'
        rep_str += '\t\\small\n'
        i = 1
        for category in categories:
            cat_weight = self._calc_subset_weight([i for i in self.own_items + self.more_items if i.category == category])
            rep_str += '\t\\ncline[arrows=-, arrowscale=1.5, nodesepB=2pt]{psChart%d}{psChartO%d}\n' % (i,i)
            rep_str += '\t\\rput[l](psChartO%d){%s}\n' % (i,category)
            rep_str += '\t\\rput(psChartI%d){%d g}\n' % (i,cat_weight)
            #rep_str += '\t\\rput(psChartI%d){%.1f \\%%}\n' % (i,self.percentages()[category])
            i += 1
        rep_str += '\t\\end{pspicture}\n\n'
        rep_str += '\t\\end{figure}\n\n'
        return rep_str
    def list_report(self):
        categories = list(set([i.category for i in self.own_items + self.more_items] + [i.category for i in self.own_items + self.more_items]))
        categories.sort()  # sorts in place
        rep_list = []
        cat_weights = {c:self._calc_subset_weight([i for i in self.own_items + self.more_items if i.category == c]) for c in categories}
        for category in categories:
            cat_weight = cat_weights[category]
            rep_list.append([category,cat_weight, '{:.1f}'.format(100*cat_weight/sum(cat_weights.values()))])
        return rep_list

