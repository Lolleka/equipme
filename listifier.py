import numpy as np
import os
import copy
import math
from tabulate import tabulate
from gearlist import gearitem, memberstat

class listifier:
    #self.n_members = 0
    #self.common_items

    def __init__(self, geardata = [], partydata = [], name=''):
        self.members = {}
        self.common_items = memberstat("Common items")
        self.name = name
        self.parse_geardata(geardata)
        if len(partydata) > 1:
            for (member_name,member_data) in partydata:
                new_memberstat = memberstat(member_name)
                new_memberstat.own_items = [gearitem(*(gi[0:3] + tuple([member_data[i]]))) for (i,gi) in enumerate(geardata) if member_data[i] > 0]
                #self.members.append(new_memberstat)
                self.members[member_name]=new_memberstat
            self.list_type = 'Party'
        else:
            self.list_type = 'Standalone'

    def parse_geardata(self, geardata):
        self.geardata = geardata
        for itemdata in geardata:
            if itemdata[3] > 0:
                self.common_items.own_items.append(gearitem(*itemdata))
    def n_members(self):
        return len(self.members)
    def add_member(self, member_name, member_data):
        new_memberstat = memberstat(member_name)
        new_memberstat.own_items = [gearitem(*(gi[0:3] + tuple([member_data[i]]))) for (i,gi) in enumerate(self.geardata) if member_data[i] > 0]
        self.members[member_name]=new_memberstat
        self.list_type = 'Party'
    def __add__ (self, other):
        tmp_list = listifier (name = self.name + ' + ' + other.name)
        
        sum_dict_items = {}
        sum_common_items = self.common_items.own_items + other.common_items.own_items 
        for gi in sum_common_items:
            if gi.name in sum_dict_items.keys():
                sum_dict_items[gi.name].quantity += gi.quantity
            else:
                sum_dict_items[gi.name] = gi

        tmp_list.common_items.own_items = list(sum_dict_items.values())

        if self.n_members() + other.n_members() > 0:
            tmp_list.members = copy.deepcopy(self.members)
            for (k,m) in other.members.items():
                if k in tmp_list.members.keys():
                    tmp_list.members[k].own_items.extend(m.own_items) 
                else:
                    tmp_list.members[k] = m
            tmp_list.list_type = 'Party'
        else:
            tmp_list.list_type = 'Standalone'
        return tmp_list

    def calculate(self):
#The following for loop takes care of distributing the common items over the party members. To do this fairly, a ledgering system is put in place, whereby on each cycle the lighter party member is assigned a new item. Then the ledger is updated for the next loop. Items are sorted in decreasing order of weight, so that it becomes increasingly easy for the algorithm to 'even out' any weight differences between all party members.
        if self.list_type == 'Party':
            for gi in self.common_items.own_items:
                if gi.quantity > 0:
                    for i in range(gi.quantity):
                        mbrs = list(self.members.values())
                        light_member = mbrs[np.argmin(list(map(lambda m: m.total_weight(), mbrs)))]
                        m_gi = light_member.get_item(gi)
                        if m_gi is None:
                            new_item = gearitem.from_gearitem(gi)
                            new_item.quantity = 1
                            light_member.more_items.append(new_item)
                        else:
                            m_gi.quantity += 1

    def report(self, print_items=False):
        rows, columns = os.popen('stty size', 'r').read().split()
        rep_str = '=' * int(columns) + '\n'
        fmt = '{:^' + columns  + '}'
        rep_str += fmt.format(self.name) + '\n'
        rep_str += '=' * int(columns) + '\n\n'

        for m in self.members.values():
            rep_str += m.report(print_items)
        if self.list_type == 'Standalone':

            ci_table = tabulate(self.common_items.list_report(),headers=['Category','Weight (g)'],tablefmt='psql')
            table_len = len(ci_table.split('\n')[0])
            rep_str += '-' * table_len + '\n'
            fmt = '{:^' + str(table_len - 2) + '}'
            rep_str += '|' + fmt.format('Common items') + '|\n'
            rep_str += ci_table + '\n\n'

            if print_items:
                categories = list(set([i.category for i in self.common_items.own_items]))
                categories.sort()  # sorts in place
                for category in categories:
                    cat_weight = self.common_items._calc_subset_weight([i for i in self.common_items.own_items if i.category == category])
                    tmp_item_list = [i for i in self.common_items.own_items if i.category == category]
                    tmp_item_list.sort(key=lambda i: i.name)
                    ci_table = tabulate([[str(i.quantity)+'x', i.name, '{:d}'.format(int(i.weight))+' g'] for i in tmp_item_list], tablefmt='rst')
                    table_len = len(ci_table.split('\n')[0])
                    #rep_str += '-' * table_len + '\n'
                    fmt = '{:^' + str(table_len) + '}'
                    rep_str +=  fmt.format(category) + '\n'
                    rep_str += ci_table + '\n\n'
        return rep_str

    def tex_report(self):

        flag = 0
        rep_str = '\\newpage\n'
        rep_str += '\\section{%s}\n\n' % str(self.name)
        if self.list_type == "Party":
            for m in self.members.values():	#Create a first table with statistics for each party member
                if flag:
                    col = 'Yellow'
                else:
                    col = 'White'
                flag = 1 - flag
                rep_str += '\t\\begin{tabular}{|l|r|}\n'
                rep_str += '\t\\hline\n'
                rep_str += '\t\\multicolumn{2}{|c|}{Panoramica pesi %s}\\\\\n' % (m.name)
                rep_str += '\t\\hline\n'
                rep_str += '\t\\rowcolor{%s}\n' % (col)
                rep_str += '\tOggetti personali: & %d\\\\\n' % (len(m.own_items))
                rep_str += '\t\\rowcolor{%s}\n' % (col)
                rep_str += '\tPeso oggetti personali: & %u g\\\\\n' % (m.own_weight())
                rep_str += '\t\\rowcolor{%s}\n' % (col)
                rep_str += '\tOggetti condivisi: & %d\\\\\n' % (len(m.more_items))
                rep_str += '\t\\rowcolor{%s}\n' % (col)
                rep_str += '\tPeso oggetti condivisi: & %u g\\\\\n' % (m.more_weight())
                rep_str += '\t\\rowcolor{%s}\n' % (col)
                rep_str += '\tTotale %s & %u g\\\\\n' % (m.name, m.total_weight())
                rep_str += '\t\\hline\n'
                rep_str += '\\end{tabular}\n\n'
                rep_str += m.tex_pie() 
                rep_str += '\\newpage\n'
                
        else:
            rep_str += '\t\\begin{tabular}{|l|r|}\n'
            rep_str += '\t\\hline\n'
            rep_str += '\t\\multicolumn{2}{|c|}{Panoramica pesi}\\\\\n'
            rep_str += '\t\\hline\n'
            rep_str += '\tTotale peso & %u g\\\\\n' % (self.common_items.total_weight())
            rep_str += '\t\\hline\n'
            rep_str += '\\end{tabular}\n\n'
            if len(self.common_items.categories()) > 1:
                rep_str += self.common_items.tex_pie() 
            
        rep_str += '\\newpage\n'
        rep_str += '\\begin{Form}\n\n'
        if self.list_type == "Party":
            for m in self.members.values():	#Create a first table with statistics for each party member
                rep_str += m.tex_report()
#End tex document and close file
        else:
            rep_str += self.common_items.tex_report()
        rep_str += '\\end{Form}\n\n'

        return rep_str

if __name__ == "__main__":
    geardata = [('formaggio','cibo',20,0), ('coltello','tools',10,0), ('caramelle','cibo',1,14)]
    partydata1 = [('lollo',np.array([1,0,0])), ('lalla', np.array([1,3,0]))]
    partydata2 = [('piero',np.array([1,0,0])), ('lalla', np.array([1,3,0]))]
    ls1 = listifier(geardata,partydata1)
    ls2 = listifier(geardata,partydata2)
    ls = ls1 + ls2
    print(ls.members['piero'].list_report())
    print(ls.members['lollo'].list_report())
    print(ls.members['lalla'].list_report())
    ls.calculate()
    print(ls.members['piero'].list_report())
    print(ls.members['lollo'].list_report())
    print(ls.members['lalla'].list_report())
    #print([str(gi) for gi in ls.members[0].more_items ])
#    print([m.name + ": " + str(m.total_weight()) for m in ls.members.values() ])

