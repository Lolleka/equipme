#!/usr/bin/env python3
#this script makes use of numpy and xlrd packages. Be sure they are properly installed in your python framework.
from numpy import *
#from xlrd import *
from subprocess import call
import os
import platform
from equipy import *
from listifier import *
from tabulate import tabulate
import argparse


# Argument parsing {{{
parser = argparse.ArgumentParser()
parser.add_argument('-n', '--nocompile', action='store_true', help='Do not compile and open the output tex file', default=False)
parser.add_argument('-i', '--items', action='store_true', help='Print out items list', default=False)
parser.add_argument('-o', '--output', nargs=1, default=['Lista.pdf'], help='Overwrite default output file')
parser.add_argument('-l', '--targetlist', nargs='*', help='List set to process (in given order)',default=[])
parser.add_argument('-s', '--sumlists', action='store_true', help='Sums list to produce an aggregated result',default=False)
parser.add_argument('-S', '--stash', nargs=1, default=['stash.json'], help='Select which stash to use') 
group = parser.add_mutually_exclusive_group()
group.add_argument('-r', '--report', action='store_true', help='Output report to terminal', default=False)
group.add_argument('-t', '--tex_report', action='store_true', help='Output tex file to terminali', default=False)

args = parser.parse_args()
excel_file = args.excel_file
stash_name = args.stash[0] 
target_lists = args.targetlist
report = args.report
tex_report = args.tex_report
nocompile = args.nocompile
output_file = args.output[0]
print_items = args.items
# }}}

exit()

print('Processing %s ...\nWorksheet: %s\nTarget lists: %s\n\n' % (excel_file, worksheet_name, target_lists))

# OLD database stuff {{{
#Opens workbook (trekking list)
#workbook = open_workbook(excel_file)
#Opens worksheet within the workbook
#worksheet = workbook.sheet_by_name(worksheet_name)

#Auxiliary variables
#n_rows = worksheet.nrows - 1
#n_cols = worksheet.ncols - 1
#
#weight = array([])  #Contains the weight for each item
#common = array([])  #Contains the common quantity for each item
#
##Fills in an auxiliary matrix, bigmatrix, for easiness of elaboration
#bigmatrix = array([]).reshape(0, n_cols+1)
#for r in range(1, n_rows+1):
#    if not worksheet.cell_type(r,0) == 0:	#Skip rows that have empty cells on the first column.
#        bigmatrix = r_[bigmatrix, [asarray(worksheet.row_values(r))]]
#
##Saves the first row of the worksheet, because it contains the party members names
#header=asarray(worksheet.row_values(0))
##Delete columns corresponding to empty strings (i.e. columns without a label)
#todelete = argwhere(header=='')
#header = delete(header, todelete.T[0], None)
#bigmatrix = delete(bigmatrix,todelete.T[0],axis=1)
#
#header_lists = header[3:]
#
##Converts empty strings to '0.0'
#bigmatrix[bigmatrix=='']='0.0'
##Sorts matrix by weight
#sorted_index=bigmatrix[:,2].astype(float).argsort()[::-1]
#bigmatrix = bigmatrix[sorted_index,:]
#
##Assign the matrix columns a new mnemonic name
#categories=list(bigmatrix[:,0]) #Containes the ordered category list
#item_names=list(bigmatrix[:,1]) #Contains the item string list
#weights=list(bigmatrix[:,2].astype(float))	#Contains the weight for each item
# }}}

list_collection = {}
for (i,col_name) in enumerate(header_lists):
    if (':' in col_name):
        (list_name, member_name) = str.split(col_name, ':')
        if (list_name in list(list_collection.keys())):
            list_collection[list_name].add_member(member_name, list(bigmatrix[:,i+3].astype(float).astype(int)))
        else:
            #new list
            geardata = zip(item_names, categories, weights, [0]*len(item_names))
            list_collection[list_name] = listifier(list(geardata), name = list_name)
            list_collection[list_name].add_member(member_name, list(bigmatrix[:,i+3].astype(float).astype(int)))
    else:
        list_name = col_name
        geardata = zip(item_names, categories, weights, list(bigmatrix[:,i+3].astype(float).astype(int)))
        if (list_name in list(list_collection.keys())):
            list_collection[list_name].parse_geardata(list(geardata))
        else:
            #new list
            list_collection[list_name] = listifier(list(geardata), name = list_name)

list_collection_keys = list(list_collection.keys())
if len(target_lists) > 0:
    for l in list_collection_keys:
        if l not in target_lists:
            del list_collection[l] 

if len(list_collection) == 0:
    exit(1)


if args.sumlists:
    l_sum = sum(list(list_collection.values()))
    list_collection = {}
    list_collection[l_sum.name] = l_sum
        

tex_file = output_file[0:-3]+'tex'
f = open( tex_file ,'w')	#Create/open a new tex file.

f.write('\\documentclass[]{article}\n')
f.write('\\usepackage{amsmath}\n')
f.write('\\usepackage{amssymb}\n')
f.write('\\usepackage{array}\n')
f.write('\\usepackage{graphicx}\n')
f.write('\\usepackage{url}\n')
f.write('\\usepackage{multirow}\n')
f.write('\\usepackage{epstopdf}\n')
f.write('\\usepackage{bm}\n')
f.write('\\usepackage{color}\n')
f.write('\\usepackage{hyperref}\n')
f.write('\\usepackage{colortbl}\n')
f.write('\\usepackage{xspace}\n')
f.write('\\usepackage{pstricks}\n')
f.write('\\usepackage{pstricks-add}\n')
f.write('\\usepackage[italian]{babel}\n')
f.write('\\usepackage[T1]{fontenc}\n')
f.write('\\usepackage[utf8]{inputenc} \n')
f.write('\\definecolor{Gray}{gray}{0.9}\n')
f.write('\\definecolor{White}{gray}{1}\n')
f.write('\\definecolor{Yellow}{rgb}{1,1,0.6}\n')
f.write('\\definecolor{LightCyan}{rgb}{0.88,1,1}\n')
f.write('\\title{Lista trekking}\n')
f.write('\\begin{document}\n')
#f.write('\\maketitle\n')
f.write('\\centering\n')
f.write('\\large\n')

for (list_name, list_item) in list_collection.items():
    list_item.calculate()
    if report:
        print(list_item.report(print_items))
#    print(list_collection[list_name].common_items.report(True))
    f.write(list_item.tex_report())

f.write('\\end{document}\n')
f.close()

if tex_report:
    f = open(tex_file,'r')
    for l in f.readlines():
        print(l.strip('\n'))
    f.close()

if not nocompile: 
    if platform.system() == 'Darwin':
        call(['xelatex',tex_file])
        call(['open',output_file])		#Mac OS
    elif platform.system() == 'Linux':
        call(["pdflatex",tex_file])
        call(["xdg-open",output_file])	#Linux/X
    elif platform.system() == 'Windows':
        call(["pdflatex",tex_file])
        os.startfile(output_file)	#Windows
